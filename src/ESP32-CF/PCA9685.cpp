// ----------------------------------------------------------------------------
// controller.ino
//
//
// Authors:
// Arwen 'interBilly' Peters
//
// Dev start 05042020
// V. 0.9.5
// ----------------------------------------------------------------------------
#include <Wire.h>
#include "PCA9685.h"




#define DEBUG false
#define TESTCODE false
#define STOPONERROR false
#define TESTLEDS false





PCA9685::PCA9685(uint8_t _address)
{
    address = _address;
    if (DEBUG)
    {
        Serial.print("PCA9685::PCA9685 constructor. \nAddress: ");
        Serial.println(address);
        Serial.print("PCA9685 MODE1 reg value: ");
        printBIN(getReg(address, MODE1_REG));
        Serial.println();
        Serial.println("Starting PCA9685");
    }

    setSleepMode(address, SLEEP_MODE_OFF);
    delay(10);
    //  Wire.beginTransmission(address);
    //  Wire.write(0x00);
    //  Wire.write(0x00);
    //  Wire.endTransmission();

    if (TESTLEDS)
    {
        testLeds();
    }
}


/*----------------------------------------------
  Public functions
  ----------------------------------------------*/
void PCA9685::setDutyCycle(uint8_t channel, uint8_t dutyCycle, uint8_t intensity)
{



    //calculate dutycycle with intensity
    dutyCycle = (uint8_t)(((double)dutyCycle / 100.0) * (double)intensity);

    //calculate off time between 0 - 4095
    uint16_t on_time = 0;
    uint16_t off_time = ((double)DUTYCYCLE_MAX / 100) * dutyCycle;

    //bitshift into high and low register
    uint8_t on_time_L = on_time;
    uint8_t on_time_H = on_time >> 8;
    //bitshift into high and low register
    uint8_t off_time_L = off_time;
    uint8_t off_time_H = off_time >> 8;

    //get channel offset from LED0_on_L register
    uint8_t reg = LED0_REG + (channel * REGISTERS_PER_CHANNEL);


    if (DEBUG)
    {
        Serial.print("\nPCA9685::setDutyCycle. \nAddress: ");
        Serial.print(address);
        Serial.print(" DC: ");
        Serial.print(dutyCycle);
        Serial.print(" off time: ");
        Serial.println(off_time);
    }

    //set on and off time, high and low registers
    setReg(address, reg + 0, on_time_L); //not using phase shift yet, only using off-time
    setReg(address, reg + 1, on_time_H); //not using phase shift yet, only using off-time
    setReg(address, reg + 2, off_time_L);
    setReg(address, reg + 3, off_time_H);
}



/*----------------------------------------------
  Register getters and setters
  ----------------------------------------------*/

uint8_t PCA9685::getReg(uint8_t addr, uint8_t reg)
{
    uint8_t tmp = -1;
    Wire.beginTransmission(addr);
    Wire.write(reg);
    Wire.endTransmission();

    Wire.requestFrom((int)addr, (int)1);
    if (Wire.available() == 1)
    {
        tmp = Wire.read();
    }
    if (tmp == -1)
    {
        Serial.println("ERROR IN " + (String)__FILE__ + ".  - getReg() failed");
        if (STOPONERROR)
            while (1);
        else
        {
            Serial.println("Resuming anyways...");
        }
    }
    return tmp;
}

uint8_t PCA9685::setReg(uint8_t addr, uint8_t reg, uint8_t value)
{
    if (DEBUG)
    {
        Serial.print("PCA9685::setReg. Addr: ");
        Serial.print(addr);
        Serial.print(". Reg: ");
        Serial.print(reg);
        Serial.print(". Value: ");
        printBIN(value);
        Serial.println();
    }

    Wire.beginTransmission(addr);
    Wire.write(reg);
    Wire.write(value);
    Wire.endTransmission();
    if (TESTCODE)
    {
        Serial.print("VERIFYING REG VALUE (");
        printBIN(getReg(addr, reg));
        Serial.println(")");
        //printBIN(value);
        Serial.println();

        if (value != getReg(addr, reg))
        {
            Serial.println("ERROR IN " + (String)__FILE__ + "PCA9685 register verification failed");
            if (STOPONERROR)
                while (1);
            else
            {
                Serial.println("Resuming anyways...");
            }
        }
    }
}

bool PCA9685::getBit(uint8_t addr, uint8_t reg, uint8_t bitNr)
{
    //get register, return masked value of bitNr'th bit
    uint8_t tmp = getReg(addr, reg);
    uint8_t mask = 0x01 << bitNr;
    uint8_t retval = tmp & mask;
    return retval >>= bitNr;
}

bool PCA9685::setBit(uint8_t addr, uint8_t reg, uint8_t bitNr, bool value)
{
    //get register, set bitNr'th bit of tmp to value and write result to register
    uint8_t tmp = getReg(addr, reg);
    tmp ^= (-value ^ tmp) & (1 << bitNr);
    setReg(addr, reg, tmp);
}

/*----------------------------------------------
  PCA9685 utility functions
  ----------------------------------------------*/
void PCA9685::resetChip(uint8_t addr)
{
    //function not used in PCA9685 object but in driver.cpp as individual reset is not required at this time
    Serial.print("Resetting chip @ adress: ");
    Serial.println(addr);
    Wire.beginTransmission(addr);
    Wire.write(0x6);
    Wire.endTransmission();
    delay(500);
}

void PCA9685::setSleepMode(uint8_t addr, bool value)
{
    setBit(addr, MODE1_REG, SLEEP_MODE_BIT, value);
}

void PCA9685::setRespondToAllCall(uint8_t addr, bool value)
{
    setBit(addr, MODE1_REG, RESPOND_TO_ALL_CALL_BIT, value);
}



/*----------------------------------------------
  Test functions
  ----------------------------------------------*/

void PCA9685::testLeds()
{
    for (int i = 0; i < 16; i++)
    {
        setDutyCycle(i, 100, 100);
    }
    delay(500);
    for (int i = 0; i < 16; i++)
    {
        setDutyCycle(i, 0, 100);
    }
    delay(50);
    setDutyCycle(0, 100, 100);
    delay(50);
    for (int i = 1; i < 16; i++)
    {
        setDutyCycle(i, 100, 100);
        setDutyCycle(i - 1, 0, 100);
        delay(50);
    }
    setDutyCycle(15, 0, 100);
}

void PCA9685::testLedsOn()
{
    for (int i = 0; i < 16; i++)
    {
        setDutyCycle(i, 100, 100);
    }
}

void PCA9685::printBIN(uint8_t input)
{
    for (int i = 7; i >= 0; i--)
    {
        if ((input & (0x01 << i)) == 0)
        {
            Serial.print("0");
        }
        else
        {
            Serial.print("1");
        }
    }
}
