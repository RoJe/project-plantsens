// ----------------------------------------------------------------------------
// Strip.cpp
//
//
// Authors:
// Arwen 'interBilly' Peters
//
// Dev start 05042020
// V. 0.3
// ----------------------------------------------------------------------------
#include  "Strip.h"

#define DEBUG true



Strip::Strip()
{
    colorMemory[0] = colorMemory[1] = colorMemory[2] = colorMemory[3] = colorMemory[4] = 0;
}

void Strip::setMem(uint8_t* input)
{
    for (i = 0; i < 5; i++)
    {
        colorMemory[i] = input[i];
        if (DEBUG)
        {
            Serial.print("Written to mem: ");
            Serial.println(colorMemory[i]);
        }
    }
}

uint8_t Strip::getMem(uint8_t i)
{
    return colorMemory[i];
}
