// ----------------------------------------------------------------------------
// controller.h
//
//
// Authors:
// Arwen 'interBilly' Peters
//
// Dev start 05042020
// V. 0.9.5
// ----------------------------------------------------------------------------
#ifndef PCA9685_H
#define PCA9685_H
#include <stdint.h>
#include <Arduino.h>

//registers
#define   MODE1_REG                   0x00
#define   MODE2_REG                   0x01
//bit numbers
#define   SLEEP_MODE_BIT              0x04
#define   RESPOND_TO_ALL_CALL_BIT     0x00
//register settings
#define   SLEEP_MODE_OFF              0x00
#define   RESPOND_TO_ALL_CALL_ON      0x01
//constants
#define   DUTYCYCLE_MAX               4095
#define   PCA9685_MAX                 0x3F
#define   LED0_REG                    0x06
#define   REGISTERS_PER_CHANNEL       0x04


class PCA9685
{
    public:

        /*----------------------------------------------
          Public variables
          ----------------------------------------------*/




        /*----------------------------------------------
          Public functions
          ----------------------------------------------*/
        PCA9685(uint8_t _address);
        void setDutyCycle(uint8_t channel, uint8_t dutyCycle, uint8_t intensity);

    private:
        /*----------------------------------------------
          Private variables
          ----------------------------------------------*/
        uint8_t address;



        /*----------------------------------------------
          Register getters and setters
          ----------------------------------------------*/
        uint8_t getReg(uint8_t addr, uint8_t reg);
        uint8_t setReg(uint8_t addr, uint8_t reg, uint8_t value);
        bool getBit(uint8_t addr, uint8_t reg, uint8_t bitNr);
        bool setBit(uint8_t addr, uint8_t reg, uint8_t bitNr, bool value);

        /*----------------------------------------------
          PCA9685 utility functions
          ----------------------------------------------*/

        void resetChip(uint8_t addr);
        void setSleepMode(uint8_t addr, bool value);
        void setRespondToAllCall(uint8_t addr, bool value);


        /*----------------------------------------------
          PCA9685 utility functions
          ----------------------------------------------*/
        void printBIN(uint8_t input);
        void testLeds();
        void testLedsOn();
};
#endif
