// ----------------------------------------------------------------------------
// PlantsensSockets
//
//
// Authors:
// Robbert de Jel
//
// V. 0.1
// ----------------------------------------------------------------------------
#ifndef  PlantSockets_h
#define  PlantSockets_h
using namespace std;

#include <stdio.h>
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <WiFiClientSecure.h>
#include <ArduinoJson.h>

#define API_SERVER "213.93.120.213"
#define API_PORT 6969
#define API_KEY "nu1UXSkLU1i4SkZQg7fdc6H30i5x7bFt"

class PlantsensSockets
{
    private:
#ifdef __AVR__
        typedef void (*OnMessEvent)(StaticJsonDocument<200>);
#else
        typedef std::function<void(StaticJsonDocument<200>)> OnMessEvent;
#endif

        char* ManagmentInstructionTable[39] =
        {
            "MicroController_Heartbeat",
            "MicroController_EnterSlaveMode",
            "MicroController_Reset",
            "MicroController_DisableAutoSequence",
            "MicroController_EnableAutoSequence",
            "WaterLevel_GetData",
            "WaterLevel_SensorTest",
            "WaterLevel_WaterInValveOpen",
            "WaterLevel_WaterInValveClose",
            "WaterLevel_WaterOutValveOpen",
            "WaterLevel_WaterOutValveClose",
            "WaterPh_GetData",
            "WaterPh_SensorTest",
            "WaterPh_PhUpPumpOn",
            "WaterPh_PhDownPumpOff",
            "WaterPh_PhDownPumpOn",
            "WaterPh_PhUpPumpOff",
            "WaterNutrient_GetData",
            "WaterNutrient_TestSensor",
            "WaterNutrient_StockAPumpOn",
            "WaterNutrient_StockAPumpOff",
            "WaterNutrient_StockBPumpOn",
            "WaterNutrient_StockBPumpOff",
            "WaterTemperature_GetData",
            "WaterTemperature_SensorTest",
            "WaterTemperature_EnableHeater",
            "WaterTemperature_DisableHeater",
            "GreenhouseOxygen_GetData",
            "GreenhouseOxygen_SensorTest",
            "GreenhouseOxygen_EnableAirflow",
            "GreenhouseOxygen_DisableAirflow",
            "GreenhouseClimate_GetData",
            "GreenhouseClimate_TestSensor",
            "AutomationControl_GetData",
            "AutomationControl_TestSensors",
            "AutomationControl_FillWaterTank",
            "LEDcontroller_setSpectrum",
            "LEDcontroller_setWhite",
            "LEDcontroller_undoWhite"
        };

        unsigned char PlantsensInstructionTable[39] =
        {
            0b00000000,
            0b00000001,
            0b00000010,
            0b00000011,
            0b00000100,
            0b00010000,
            0b00010001,
            0b00010010,
            0b00010011,
            0b00010100,
            0b00010101,
            0b00100000,
            0b00100001,
            0b00100010,
            0b00100011,
            0b00100100,
            0b00100101,
            0b00110000,
            0b00110001,
            0b00110010,
            0b00110011,
            0b00110100,
            0b00110101,
            0b01000000,
            0b01000001,
            0b01000010,
            0b01000011,
            0b01010000,
            0b01010001,
            0b01010010,
            0b01010011,
            0b01100000,
            0b01100001,
            0b01110000,
            0b01110001,
            0b01110010,
            0b10000001,
            0b10000010,
            0b10000011
        };
        const char* root_cert = \
                                "-----BEGIN CERTIFICATE-----\n" \
                                "MIICrzCCAZcCFCXcIrmNuGEjXzGSOCndIJr5+dlLMA0GCSqGSIb3DQEBCwUAMBQx\n" \
                                "EjAQBgNVBAMMCWxvY2FsaG9zdDAeFw0yMDA1MzAxNTUzMzVaFw0yMTA1MzAxNTUz\n" \
                                "MzVaMBQxEjAQBgNVBAMMCWxvY2FsaG9zdDCCASIwDQYJKoZIhvcNAQEBBQADggEP\n" \
                                "ADCCAQoCggEBAO7XJcJX756z0IjvuyzStxrt8bRyhVN8rA9CdN3p8blzJbWJ4Ro5\n" \
                                "g2DBr7ihSViYnqowdjnBRZnnRiQ2Joenmwieh+CZNLMSmOTIS3T8HVwvUoUK67fV\n" \
                                "qbr/Vcafu5RRVeUrGmY8emSuvLGXlMWc8+jhEJ1KHXrOPyAqBDucO2J5p3CLZvA5\n" \
                                "YaQQp+lH0NEFgkjxGaEKX7bJmDZp7k9eg1MSND5ZDhdUKV+vmxpd+7K5sbGB/SdS\n" \
                                "g5wvEPTpPuglZ/ICabkAcPkkkM1+WVITpPYFfd/NHjLGkpLtII/60F/PkBi2UCjs\n" \
                                "Q877j57L9Nbr1a2HRP92GK/3qBg21ATTZHUCAwEAATANBgkqhkiG9w0BAQsFAAOC\n" \
                                "AQEAbME4qFuhATyDCpLeNFSeh42E1E8Jv1lmJrwdLaGkdt2OhaY3gpMh0U8d8nCH\n" \
                                "tsERcCQK+bSLmYN+qO4lZMMe1BMpjHGa4PhtF/BAJBjqvE/87lhV4oxp0IUdNtcs\n" \
                                "62Sm4PRJgInYNciLwzAVgHWGBsgEZnNEVQS0YQFvUA6KQsKDenisYe/3EuLLqMNo\n" \
                                "dX4TgQpal+/dEXN9F4H5VJclozlj/NRm1kdjHguiWjvqf1YgV4biErsYY30P5/qw\n" \
                                "ttFMGPI/yaog3Qtr/dU3UJsZJE2vM54HHYT54YyThNH0ECc47v9GSOy0MFdUm8Dg\n" \
                                "nfwkr7uSR2ctYuWlrVGpxMhJMA==\n" \
                                "-----END CERTIFICATE-----\n";

        const char* client_priv_key = \
                                      "-----BEGIN RSA PRIVATE KEY-----\n" \
                                      "MIIEpAIBAAKCAQEA7tclwlfvnrPQiO+7LNK3Gu3xtHKFU3ysD0J03enxuXMltYnh\n" \
                                      "GjmDYMGvuKFJWJieqjB2OcFFmedGJDYmh6ebCJ6H4Jk0sxKY5MhLdPwdXC9ShQrr\n" \
                                      "t9Wpuv9Vxp+7lFFV5SsaZjx6ZK68sZeUxZzz6OEQnUodes4/ICoEO5w7YnmncItm\n" \
                                      "8DlhpBCn6UfQ0QWCSPEZoQpftsmYNmnuT16DUxI0PlkOF1QpX6+bGl37srmxsYH9\n" \
                                      "J1KDnC8Q9Ok+6CVn8gJpuQBw+SSQzX5ZUhOk9gV9380eMsaSku0gj/rQX8+QGLZQ\n" \
                                      "KOxDzvuPnsv01uvVrYdE/3YYr/eoGDbUBNNkdQIDAQABAoIBAHwJKafjKSMfORZU\n" \
                                      "gb1GdSc8GUFgFBOGdIi8N5sjqpBn11aPE2MeKTdvfwWAZWnoYMX7wvo9gBrsO8YE\n" \
                                      "hT//8Aiq49/lULQK3XI6/szzPYApZ06GnGHtSWc+wVoUjEL7doM8PkqH7/wnrwD/\n" \
                                      "uphh6V2wvIMjAWpMgAYBLA+VFFYP3Or6Ke5P/ImQSqYSmja3fgdyD3apeyydca8q\n" \
                                      "X7YwVRVwosbje2KkFlymGK5HmFpWa9SP7VXbvA7W9/nrNIYu19MGLwGABj2xq6rk\n" \
                                      "d9wrEU7iwwr5rp6NKuha396vjvsCELFwNv0nW5PF+Q8CAKcFCq8BROTWY7OU6p4Y\n" \
                                      "Nv19ZEECgYEA95F+L6Zek7dA1woUZI9GSwD+QlNyHxpLVATgMDj72BiUEx0GltDJ\n" \
                                      "Wzo3G3aP8ECIKaVVyytz2UxRJsF8geqtgMoWfy8xFh7vmR9kgHn+pODNf0jtEXo2\n" \
                                      "76FgpLd7jk3g8Ay42X9gXvztLWupGBje3BBcz95foJ1t6txg3ZcMtwUCgYEA9vmO\n" \
                                      "r+jwSZO/vZGLlvhQqhPFJcddCwTItq8LT8ztsJfYotKF70SPeefZiH1zJepS4ZnF\n" \
                                      "FseUIEzSBFbrC9oVa/O2rXUMyUuPInGtqizC80vgz7VyANHlSjECvsq21KfUxJdB\n" \
                                      "JdbU8kDUyK7BHPwbVCEcf4IjQmP2hgomHtKIkrECgYEAzdWI/eMsdVKCtYfq5qT9\n" \
                                      "A7VisRCiLNv2z3dyUoW+VrAwxSd36ALVt2TSlqHgERgGXeHaUB7jpkK+oiNy+q18\n" \
                                      "7jyXsc0x2buE6GHidLN5lepW0UJ34PUlId2h9bGcJ/f2Un+xXRx7qt5N8Fq375IW\n" \
                                      "ERAFqsqv5WGfFwt9+h7xcmUCgYAwDUgMw8aUmjGRIZNGMVQIc0ZFXm+k6C5hFNef\n" \
                                      "lCCe3jxhGZLDhdRghJKXcskHlHrBuBhXoUcZrbii7YJX0PugAFAHmaF+PLxBmFqQ\n" \
                                      "pJrdmWLJJuVicXQqJjUBiBFgeLL1+PPM9+qAo8qCrTWXfRmYGcUgreJMVYSueaUH\n" \
                                      "FDFMUQKBgQCqPc7clLzW+7PhhN+GQIyAAwjJ+7niAEpEDeORs5BE+m2qhceTVvAf\n" \
                                      "wtC/Ta1NUmStXUVDhu0kpo/tg2CFE9mvPjb1NTu8nXr8nphMldwlOOANzomr2htp\n" \
                                      "D1U6fhzXQ0ldjHIXK+QhPlXUr0RgufPCIZ8Dj0rrlGnWLZArWFMBIw==\n" \
                                      "-----END RSA PRIVATE KEY-----\n";
        WiFiClientSecure client;
        OnMessEvent _OnMessageEventFunc;

        std::string buffer;

        void Connect();
    public:
        PlantsensSockets();
        void Loop();
        void AddOnMessageEventFunc(OnMessEvent);
        void GetOnMessageEventFunc(StaticJsonDocument<200>);
        void SendMessage(string);
        string GenerateResponse(unsigned char, char*);
        unsigned char TranslateTypeToPlant(string);
        char* TranslateTypeToManagement(unsigned char);
        void OnFailedEvent();
};


#endif
