// ----------------------------------------------------------------------------
// Driver.h
//
//
// Authors:
// Arwen 'interBilly' Peters
//
// Dev start 05042020
// V. 0.8
// ----------------------------------------------------------------------------
#ifndef  Driver_h
#define  Driver_h


#include <stdint.h>
#include "Strip.h"


#define channelsPerPCA9685 16
#define color_amt         4         //amt of colors on ledstrip
#define baseAddressPCA9685 0x40


class Driver
{
    public:
        Driver(uint8_t strip_amt);
        uint8_t setSpectrum(uint8_t strip_nr, uint8_t* spectrum, bool writeMem);
        uint8_t setWhite(uint8_t intensity);
        uint8_t undoWhite();
        uint8_t reply();
        //uint8_t PCA9685_nr;

    private:
        uint8_t strip_amt;
        //uint8_t strip_nr;
        uint8_t channel_offset_nr;
        uint8_t PCA9685_amt;
        uint8_t colorArray_driver[5];
        uint8_t i;
        uint8_t stripsPerPCA9685;
};

#endif
