// ----------------------------------------------------------------------------
// Strip.h
//
//
// Authors:
// Arwen 'interBilly' Peters
//
// Dev start 05042020
// V. 0.3
// ----------------------------------------------------------------------------
#ifndef  STRIP_H
#define  STRIP_H
#include <stdint.h>
#include <Arduino.h>

class Strip
{
    public:
        Strip();
        void setMem(uint8_t* input);
        uint8_t getMem(uint8_t i);
        uint8_t colorMemory[5] = {0, 0, 0, 0, 0};

    private:
        uint8_t i;

};

#endif
