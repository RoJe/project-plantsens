// ----------------------------------------------------------------------------
// plantWifi
//
//
// Authors:
// Robbert de Jel
//
// V. 0.1
// ----------------------------------------------------------------------------
#ifndef  PlantWifi_h
#define  PlantWifi_h
#include <stdio.h>
//#include <iostream>
#include <string.h>
#include <stdlib.h>
#include "WiFiClientSecure.h"

#define SSID "Betula"
#define PASSWORD "lj%2Iy*mm4^2uxRt1Bh^bB9Nkn62gSi5"

typedef void (*PlantWifiConnectEvent)(void);
typedef void (*PlantWifiDisconnectEvent)(void);

class PlantWifi
{
    private:
        WiFiClientSecure client;

        void Connect();
    public:
        PlantWifiConnectEvent pwConnectEvent;
        PlantWifiDisconnectEvent pwDisconnectEvent;
        PlantWifi();
        void Loop();

        void AddConnectEvent(PlantWifiConnectEvent);
        void OnConnectEvent();
        void AddDisconnectEvent(PlantWifiDisconnectEvent);
        void OnDisconnectEvent();
};

#endif
