// ----------------------------------------------------------------------------
// Driver.cpp
//
//
// Authors:
// Arwen 'interBilly' Peters
//
// Dev start 05042020
// V. 0.9.6
// ----------------------------------------------------------------------------
#include <stdint.h>
#include "Driver.h"
#include "PCA9685.h"
#include <Wire.h>
#include <string.h>


#define DEBUG true


//PCA9685* PCA9685arrayPointer;
PCA9685* PCA9685array[64]; //OBJECT BUG, make init amt dynamic


//Strip* stripArrayPointer;
Strip* stripArray[4 * 4]; //OBJECT BUG, make init amt dynamic



Driver::Driver(uint8_t _strip_amt)
{
    uint8_t PCA9685_amt = channel_offset_nr = i = stripsPerPCA9685 = colorArray_driver[0] = colorArray_driver[1] = colorArray_driver[2] = colorArray_driver[3] = colorArray_driver[4] = 0;

    //calculate strip_amt
    strip_amt = _strip_amt;
    stripsPerPCA9685 = (uint8_t)(channelsPerPCA9685 / color_amt);
    PCA9685_amt = (uint8_t) (strip_amt / stripsPerPCA9685);

    if (strip_amt % stripsPerPCA9685 > 0)
    {
        PCA9685_amt++;
    }

    if (DEBUG)
    {
        Serial.print("Driver::Driver constructor. \n_strip_amt: ");
        Serial.print(strip_amt);
        Serial.print(". PCA9685_amt: ");
        Serial.println(PCA9685_amt);
    }


    if (PCA9685_amt > 63)
    {
        Serial.println("ERROR IN " + (String)__FILE__ + ". - Invalid PCA9685_amt");
        return;
    }


    //reset PCA9685's
    if (DEBUG)
    {
        Serial.println("Resetting PCA9685's to default settings");
    }
    Wire.beginTransmission(0x0);
    Wire.write(0x6);
    Wire.endTransmission();
    delay(500);


    //create PCA9685 objects
    //  PCA9685* PCA9685array[PCA9685_amt];
    //PCA9685arrayPointer = *PCA9685array;
    for (uint8_t PCA9685_nr = 0; PCA9685_nr < PCA9685_amt; PCA9685_nr++)
    {
        PCA9685array[PCA9685_nr] = new PCA9685((baseAddressPCA9685 + PCA9685_nr));
    }



    //create strip objects for color memory
    //  Strip* stripArray[strip_amt];
    //stripArrayPointer = *stripArray;
    for (uint8_t strip_nr = 0; strip_nr < strip_amt; strip_nr++)
    {
        stripArray[strip_nr] = new Strip();
    }
}

uint8_t Driver::setSpectrum(uint8_t strip_nr, uint8_t* colorArray, bool writeMem)
{
    if (DEBUG)
    {
        Serial.println("\n\nDriver::setSpectrum");
        Serial.println("Passing color values:");
        for (int x = 0; x < color_amt; x++)
        {
            Serial.println(colorArray[x]);
        }
        Serial.println(colorArray[4]);
        Serial.println("");
    }

    //calculate PCA9685_nr and channel_offset_nr
    uint8_t PCA9685_nr = (uint8_t)(strip_nr / stripsPerPCA9685);
    uint8_t channel_offset_nr = (strip_nr - (PCA9685_nr * stripsPerPCA9685)) * color_amt;

    //set strip individual dutycycle and pass intensity (array[4])
    for (int y = 0; y < color_amt; y++)
    {
        PCA9685array[PCA9685_nr]->setDutyCycle(channel_offset_nr + y, colorArray[y], colorArray[4]);
    }


    //write dutycycles to memory or not
    if (writeMem)
    {
        //stripArrayPointer[strip_nr].setMem(colorArray);
        //stripArray[strip_nr].setMem(colorArray);
        stripArray[strip_nr]->setMem(colorArray);
    }
    if (DEBUG)
    {
        Serial.println("setSpectrum done.");
    }
    return 0;
}



uint8_t Driver::setWhite(uint8_t intensity)
{
    //set all colors to 100%
    colorArray_driver[0] = colorArray_driver[1] = colorArray_driver[2] = colorArray_driver[3] = 100;
    //set intensity
    colorArray_driver[4] = intensity;
    //set dutycycles to all strips, dont save to memory
    for (uint8_t strip_nr = 0 ; strip_nr < strip_amt; strip_nr++)
    {
        setSpectrum(strip_nr, colorArray_driver, false);
    }
    return 0;
}

uint8_t Driver::undoWhite()
{
    //for all strips
    for (uint8_t strip_nr = 0 ; strip_nr < strip_amt; strip_nr++)
    {
        //get dutycycles from memory
        for (i = 0; i < 5; i++)
        {
            //colorArray_driver[i] = stripArray[strip_nr].getMem(i);
            colorArray_driver[i] = stripArray[strip_nr]->getMem(i);
        }
        //set dutycycle of current strip
        if (DEBUG)
        {
            Serial.println("Undoing white!!");
            for (int x = 0; x < 5; x++)
            {
                Serial.println(colorArray_driver[x]);
            }
            Serial.println("END");
        }
        setSpectrum(strip_nr, colorArray_driver, false);
    }
    return 0;
}
