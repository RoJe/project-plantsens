#include "PlantsensSockets.h"

using namespace std;


PlantsensSockets::PlantsensSockets()
{
    Connect();
}
string PlantsensSockets::GenerateResponse(unsigned char typeBinary, char* dataString)
{
    // allocate the memory for the document
    StaticJsonDocument<200> doc;

    // create an object
    JsonObject object = doc.to<JsonObject>();
    object["name"] = "Responce";
    object["type"] = TranslateTypeToManagement(typeBinary);
    object["data"] = dataString;

    string output;
    // serialize the object and send the result to Serial
    serializeJson(doc, output);
    return output;
}
void PlantsensSockets::Connect()
{
    cout << "Starting socket connection..." << endl;
    client.setCertificate(root_cert); // for client verification
    client.setPrivateKey(client_priv_key);	// for client verification
    while (!client.connect(API_SERVER, API_PORT))
    {
        cout << "trying...." << endl;
        OnFailedEvent();
        delay(1000);
    }
    cout << "Connected to server! Sending key...." << endl;
    client.println(API_KEY);
}
void PlantsensSockets::AddOnMessageEventFunc(OnMessEvent _func)
{
    _OnMessageEventFunc = _func;
}
void PlantsensSockets::GetOnMessageEventFunc(StaticJsonDocument<200> msg)
{
    if (_OnMessageEventFunc)
    {
        _OnMessageEventFunc(msg);
    }
}

unsigned char PlantsensSockets::TranslateTypeToPlant(string t)
{
    for (int i = 0; i < 39; i++)
    {
        if(ManagmentInstructionTable[i] == t)
        {
            return PlantsensInstructionTable[i];
        }
    }
}
char* PlantsensSockets::TranslateTypeToManagement(unsigned char t)
{
    for (int i = 0; i < 39; i++)
    {
        if(PlantsensInstructionTable[i] == t)
        {
            return ManagmentInstructionTable[i];
        }
    }
}

void PlantsensSockets::Loop()
{

    if (client.available())
    {
        buffer += client.read();
        StaticJsonDocument<200> doc;
        DeserializationError error = deserializeJson(doc, buffer);
        if(!error)
        {
            GetOnMessageEventFunc(doc);
            buffer = "";
        }

    }
    if(WiFi.status() != WL_CONNECTED)
    {
        Connect();
    }
}

void PlantsensSockets::SendMessage(string msg)
{
    char tmp[msg.length() + 1];
    strcpy(tmp, msg.c_str());

    if (client.println(tmp) || client.println(tmp))
    {
        cout << tmp << endl;
    }

}
void PlantsensSockets::OnFailedEvent()
{

}
