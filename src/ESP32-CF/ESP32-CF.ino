/*¤ø,¸¸,ø¤º°`°º¤ø,¸,ø¤°º¤ø,¸¸,ø¤º°`°º¤ø,¸¸,ø¤º°`°º¤ø,¸,ø¤°º¤ø,¸¸,ø¤¤ø,¸¸,ø¤º°`°º¤ø,¸,ø¤°º¤ø,¸¸,ø¤º°`°º¤ø,¸¸,ø¤º°`°º¤ø,¸,ø¤°º¤ø,¸¸,ø¤


ESP32-CF.ino

Authors:
Arwen 'interBilly' Peters
Joost 'MrToast' Van Brakel  - WIFI & Websocket functionalities
Robbert 'Roje' De Jel       - WIFI & Websocket functionalities

Dev start 05052020
V. 0.7

This is the ESP-32 combined functionalities program, it merges multiple functionalities to be utilized for a hydroponics system.

Files:
  ESP32-CF              - This is the ESP-32 combined functionalities program, it merges multiple functionalities to be utilized for a hydroponics system.
  Driver.cpp            - Driver class, to be made into an object once as an interface to the LED controller. Initialised with variables that facilitate scaleability (strip_amt, color_amt).
  Driver.h              - Accompanying header file.
  PCA9685.cpp           - PCA9685 class, one object is made for each PCA9685 IC used.
  PCA9685.h             - Accompanying header file.
  Strip.cpp             - Strip class holds current spectrum values facilitating setWhite() and undoWhite() functionalities.
  Strip.h               - Accompanying header file.
  PlantSockets.cpp      - Facilitating websocket functionalities
  PlantSockets.cpp      - Accompanying header file.
  PlantWifi.cpp         - Facilitating WIFI functionalities
  PlantWifi.cpp         - Accompanying header file.
  commlib.cpp           - Library facilitating communication.
  commlib.h             - Accompanying header file.


¤ø,¸¸,ø¤º°`°º¤ø,¸,ø¤°º¤ø,¸¸,ø¤º°`°º¤ø,¸¸,ø¤º°`°º¤ø,¸,ø¤°º¤ø,¸¸,ø¤¤ø,¸¸,ø¤º°`°º¤ø,¸,ø¤°º¤ø,¸¸,ø¤º°`°º¤ø,¸¸,ø¤º°`°º¤ø,¸,ø¤°º¤ø,¸¸,ø¤*/

using namespace std;


// enable/disable wifi
#define WIFI true

// enable/disable websocket
#define WEBSOCKET true

// enable/disable SPI listening
#define SPILISTEN true

// enable/disable testing code in loop
#define TESTINGSTUFF false

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ArduinoJson.h>
#include "commlib.h"
#include <Wire.h>
#include "Driver.h"
//#include <iostream.h>


// When wifi do..
#if WIFI
    #include "PlantWifi.h"
    PlantWifi* plantWifiPointer;
#endif

// When websocket do..
#if WEBSOCKET
    #include "PlantsensSockets.h"
    PlantsensSockets* plantSocketPointer;
#endif

uint8_t strip_amt = 8;
Driver* driverPointer;
bool oneshot = true;

void setup()
{
    Serial.begin(115200);
    cout << "\nESP-CF" << endl;
    cout << "Compiled: " << __DATE__ << ", " << __TIME__ << "." << endl;
    delay(250);
    Serial.print("Starting");
    for (int i = 0; i < 3; i++)
    {
        delay(250);
        Serial.print(".");
        delay(250);
    }
    Serial.println();


#if WIFI
    plantWifiPointer = new PlantWifi();
#endif

    // enable/disable websocket
#if WEBSOCKET

    plantSocketPointer = new PlantsensSockets();
    plantSocketPointer->AddOnMessageEventFunc(HandleManagementJson);
#endif

    //Wire.setClock(100 000); //obsolete
    Wire.begin(21, 22);      //TODO move I2C related stuff to comm lib.


    driverPointer = new Driver(strip_amt);
    //share necessary variables with commlib and setup SPI
    setupCommunication(driverPointer);
}



void loop()
{
#if WIFI
    plantWifiPointer->Loop();
#endif

    // enable/disable websocket
#if WEBSOCKET
    plantSocketPointer->Loop();
#endif

    //check for incoming from below
#if SPILISTEN

    uint8_t returnPointer[100];
    bool data_ready = SPIlisten(returnPointer);
    if (data_ready)
    {
        uint8_t bytesRemaining = returnPointer[0]; //data + 2
        uint8_t type = returnPointer[1];
        uint8_t dataArray[bytesRemaining - 2];
        int i;
        for (i = 0; i < bytesRemaining - 2; i++)
        {
            dataArray[i] = returnPointer[i + 2];
        }
#if WEBSOCKET
        plantSocketPointer->SendMessage(plantSocketPointer->GenerateResponse(type, GenerateReturnDataString(dataArray, i)));
#endif
    }
#endif
#if TESTINGSTUFF
    //testing stuff
    //uint8_t msg[] = {100, 100, 100, 100, 100};

    //driverPointer->setWhite(100);
    //delay(1000);
    //driverPointer->setSpectrum(7, msg, true);

    //  driverPointer->setSpectrum(0, msg, true);
    //  driverPointer->setSpectrum(1, msg, true);
    //  driverPointer->setSpectrum(4, msg, true);
    //  driverPointer->setSpectrum(5, msg, true);
    //  delay(1000);

    //  delay(1000);
    //  driverPointer->undoWhite();
    //  while(1);

    if (oneshot)
    {
        oneshot = false;
        char msg[] = {1,0};
        testSPI(msg, sizeof(msg)/sizeof(msg[0]) );
    }

    while (1);
#endif
}



void HandleManagementJson(StaticJsonDocument<200> jsonData)
{
    char* response;
    string typeString = jsonData["type"];
    string dataString = jsonData["data"];
    char typeStringCharArray[typeString.length() + 1];
    char dataStringCharArray[dataString.length() + 1];
    unsigned char typeBinary = plantSocketPointer->TranslateTypeToPlant(jsonData["type"]);

    cout << typeString << endl;
    cout << dataString << endl;
    strcpy(typeStringCharArray, typeString.c_str());
    strcpy(dataStringCharArray, dataString.c_str());
    decodeMessage(typeStringCharArray, dataStringCharArray, typeBinary);
}



//uint8_t* sendPic(uint8_t type, uint8_t* Data)
//{
//
////  Serial.print("sendPic: Type: ");
////  Serial.print(type);
////  Serial.print(" Data: ");
////  Serial.println(Data[0]);
////
////  char Str[] = "JoostIsKut";
////  char rply[11];
////  PIC16->beginTransaction(SPISettings(spiClk, MSBFIRST, SPI_MODE0));
//////  uint8_t reply1 = PIC16->transfer(type);
//////  uint8_t reply2 = PIC16->transfer(Data[0]);
////
////
////  for(int i=0;i<10;i++)
////  {
////    Serial.write(PIC16->transfer(Str[i]));
////  }
////  Serial.write(PIC16->transfer(0));
////  Serial.println();
////
////  PIC16->endTransaction();
////
////
////
////
////
////
////
//  static uint8_t temp[10];
//  temp[0] = (uint8_t)5;
//  temp[1] = (uint8_t)129;
//  return temp;
//}
//
//
//uint8_t* sendLedController(uint8_t type, uint16_t Led, uint8_t* Data)
//{
//  //Driver driver = Driver(strip_amt);
//
//  Serial.print("sendLedController: Type: ");
//  Serial.print(type);
//  Serial.print(" LED: ");
//  Serial.print(Led);
//  Serial.print(" Data: ");
//  Serial.println(Data[0]);
//
//
//  uint8_t mask = 0b1111;
//  uint8_t actionParam = type & mask;
//  switch (actionParam)
//  {
//    case 0: //init
//      break;
//
//    case 1: //setSpectrum
//      Serial.println("CASE 1 setspectrum");
//      driver.setSpectrum(Led, Data, true);
//
//      break;
//
//    case 2: //setWhite
//      driver.setWhite((uint8_t)Led);
//      Serial.print("(uint8_t)Led: ");
//      Serial.println((uint8_t)Led);
//      break;
//
//    case 3: //undoWhite
//      driver.undoWhite();
//      break;
//  }
//
//
//
//
//  static uint8_t temp[10];
//  temp[0] = (uint8_t)5;
//  temp[1] = (uint8_t)129;
//  return temp;
//}
