#include "PlantWifi.h"
#include <iostream>
using namespace std;

PlantWifi::PlantWifi()
{

    Connect();
}


void PlantWifi::Connect()
{
    cout << "Starting wifi connection" << endl;
    WiFi.begin(SSID, PASSWORD);
    while (WiFi.status() != WL_CONNECTED)
    {
        cout << "Connecting..." << endl;
        delay(1000);
    }
    cout << "Connected to:" << SSID << endl;
    OnConnectEvent();
}


void PlantWifi::Loop()
{
    if(WiFi.status() != WL_CONNECTED)
    {
        OnDisconnectEvent();
        Connect();
    }
}



void PlantWifi::OnConnectEvent()
{

}


void PlantWifi::OnDisconnectEvent()
{

}
