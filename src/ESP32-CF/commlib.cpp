// ----------------------------------------------------------------------------
// communication.cpp
//
//
// Authors:
// Joost 'MrToast' Van Brakel
// Arwen 'interBilly' Peters
//
// Dev start 09052020
// V. 0.3
// ----------------------------------------------------------------------------
using namespace std;
#include <Arduino.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include "commlib.h"
#include "Driver.h"

#define DEBUG true
#define SPI_INTERRUPT_PIN   15
#define SPI_DELAY           32

#include <SPI.h>

static const int spiClk = 100000; // !(1 MHz)
SPIClass* PIC16 = NULL;


Driver* driverPointerC;

void setupCommunication(Driver* driverPointer)
{
    pinMode(SPI_INTERRUPT_PIN, INPUT);

    driverPointerC = driverPointer;
    PIC16 = new SPIClass(VSPI);
    PIC16->begin();   //SCLK = 18, MISO = 19, MOSI = 23
}

void decodeMessage(char* typeString, char* dataString, unsigned char typeBinary)
{

    uint8_t* Data;
    size_t arraylenght = 0;

    if ((typeBinary & 0xF0) == 0x80)
    {
        uint16_t Led = 0;
        Data = GetDataLed(dataString, &Led);
        sendLedController(typeBinary, Led, Data, &arraylenght);

    }
    else
    {
        Data = GetData(dataString);
        sendPic(typeBinary, Data, &arraylenght);
    }
}

char* splitStrings(char* messageString, char* splitString)
{
    char* SplitStrings = NULL;
    SplitStrings = strtok(messageString, splitString);
    return SplitStrings;
}

uint8_t* GetDataLed(char* DataString, uint16_t* Led)
{
    if (DataString == NULL)
    {
        return NULL;
    }

    static uint8_t Data[10];
    memset(Data, 0, 10);

    char DataStringFixed[strlen(DataString) - 1];
    for (int i = 1; i < strlen(DataString) - 1; i++)
    {
        DataStringFixed[i - 1] = DataString[i];
    }
    DataStringFixed[strlen(DataString) - 2] = '\0';

    if (strstr(DataStringFixed, ", ") != NULL)
    {
        char* DataStringSplit = splitStrings(DataStringFixed, ", ");
        int i = 0;
        uint8_t first = 0;
        while (DataStringSplit != NULL)
        {
            if (first == 0)
            {
                *Led = (uint16_t)atoi(DataStringSplit);
                first = 1;
            }
            else
            {
                Data[i] = (uint8_t)atoi(DataStringSplit);
                i++;
            }
            DataStringSplit = strtok(NULL, ", ");
        }
    }
    else
    {
        Data[0] = (uint8_t)atoi(DataStringFixed);
    }

    return Data;
}

uint8_t* GetData(char* DataString)
{
    if (DataString == NULL)
    {
        return NULL;
    }

    static uint8_t Data[10];
    memset(Data, 0, 10);

    char DataStringFixed[strlen(DataString) - 1];
    for (int i = 1; i < strlen(DataString) - 1; i++)
    {
        DataStringFixed[i - 1] = DataString[i];
    }
    DataStringFixed[strlen(DataString) - 2] = '\0';
    if (strstr(DataStringFixed, ", ") != NULL)
    {
        char* DataStringSplit = splitStrings(DataStringFixed, ", ");
        int i = 0;
        while (DataStringSplit != NULL)
        {
            Data[i] = (uint8_t)atoi(DataStringSplit);
            i++;
            DataStringSplit = strtok(NULL, ", ");
        }
    }
    else
    {
        Data[0] = (uint8_t)atoi(DataStringFixed);
    }
    return Data;
}

char* GenerateReturnDataString(uint8_t* ReturnData, size_t arraylength)
{

    int len = arraylength * 5;
    char* ReturnDataString = (char*) malloc (len);
    ReturnDataString[0] = '[';
    int CharCount = 1;
    for (int j = 0; j < arraylength; j++)
    {
        char TempString[3];
        itoa(ReturnData[j], TempString, 10);
        for (int k = 0; k < strlen(TempString); k++)
        {
            ReturnDataString[CharCount] = TempString[k];
            CharCount++;
        }
        if (j != arraylength - 1)
        {
            ReturnDataString[CharCount] = ',';
            CharCount++;
            ReturnDataString[CharCount] = ' ';
            CharCount++;
        }
    }
    ReturnDataString[CharCount] = ']';
    CharCount++;
    ReturnDataString[CharCount] = '\0';
    return ReturnDataString;
}


void sendPic(uint8_t type, uint8_t* Data, size_t* __arrayLength)
{
    uint8_t arrayLength = *__arrayLength;
    if ((arrayLength < 0) || (arrayLength > 7))
    {
        Serial.println("ERROR: Invalid arrayLength. (" + (String)arrayLength + ")");
        while (1);
    }
    PIC16->beginTransaction(SPISettings(spiClk, MSBFIRST, SPI_MODE0));

    //send total length and type
    PIC16->transfer(arrayLength + 1);
    delay(SPI_DELAY);
    PIC16->transfer(type);

    //send data array
    for (int i = 0; i < arrayLength; i++)
    {
        delay(SPI_DELAY);
        PIC16->transfer(Data[i]);
    }
    delay(SPI_DELAY);

    PIC16->endTransaction();
}


bool SPIlisten(uint8_t* returnData)
{
    //LATC5 active high interrupt pin from PIC16f1829 is connected to the G17 pin on the ESP32
    //if (DEBUG) Serial.println("communication::SPIlisten");

    if (!digitalRead(SPI_INTERRUPT_PIN))
    {
        return false;
    }
    else
    {
        if (DEBUG)
        {
            Serial.println("SPILISTEN triggered");
        }
        //get datalength x
        uint8_t bytesRemaining = PIC16->transfer(0); //receive remaining bytes to read (type (1) + data (x) )
        delay(SPI_DELAY);

        if (DEBUG)
        {
            Serial.println("bytesRemaining: " + (String)bytesRemaining);
        }

        if ((bytesRemaining < 1) || (bytesRemaining > 8))
        {
            Serial.println("ERROR: Invalid bytesRemaining. (" + (String)bytesRemaining + ")");
            //while (1);
            return false;
        }
        uint8_t incomingData[bytesRemaining];

        if (DEBUG)
        {
            Serial.println("Receiving data (" + (String)bytesRemaining + ") : ");
        }

        //    //get type
        //    uint8_t type = PIC16->transfer(0);
        //    //make returndata array for rest of bytes (x-1)
        //    uint8_t returnData[bytesRemaining - 1];
        //    for (int i = 0; i < bytesRemaining - 1; i++)
        //    {
        //      returnData[i] = PIC16->transfer(0);
        //      if (DEBUG) Serial.print(returnData[i]);
        //    }

        //get type

        //make returndata array for rest of bytes (x-1)
        //uint8_t returnData[bytesRemaining + 1];

        returnData[0] = bytesRemaining + 1;

        PIC16->beginTransaction(SPISettings(spiClk, MSBFIRST, SPI_MODE0));
        for (int i = 1; i < bytesRemaining + 1; i++)
        {
            delay(SPI_DELAY);
            returnData[i] = PIC16->transfer(0);
            if (DEBUG)
            {
                Serial.print(returnData[i]);
            }
        }
        PIC16->endTransaction();

        //length, type, data
        if (DEBUG) for (int i = 0; i < (sizeof(returnData) / sizeof(returnData[0])); i++ )
            {
                Serial.println("\nElement:" + (String)i + " . of array is:" + (String)returnData[i] + ".");
            }

        return true;
    }
}

void sendLedController(uint8_t type, uint16_t Led, uint8_t* Data, size_t* arraylength)
{
    Serial.print("sendLedController: Type: ");
    Serial.print(type);
    Serial.print(" LED: ");
    Serial.print(Led);
    if (Data != NULL)
    {
        Serial.print(" Data: ");
        Serial.println(Data[0]);
    }
    Serial.println();


    uint8_t mask = 0b1111;
    uint8_t actionParam = type & mask;
    static uint8_t resp[1];
    switch (actionParam)
    {
        case 0: //init
            break;

        case 1: //setSpectrum
            Serial.println("CASE I setspectrum");
            resp[0] = driverPointerC->setSpectrum(Led, Data, true);

            break;

        case 2: //setWhite
            Serial.println("CASE II setWhite");
            resp[0] = driverPointerC->setWhite(Data[0]);
            Serial.print("(uint8_t)Led: ");
            Serial.println((uint8_t)Led);
            break;

        case 3: //undoWhite
            Serial.println("CASE III undoWhite");
            resp[0] = driverPointerC->undoWhite();
            break;
    }


    Serial.println(resp[0]);
    *arraylength = sizeof(resp) / sizeof(uint8_t);
}

void testSPI(char* input, int arraySize)
{

    Serial.println("testSPI");
    uint8_t msgLength = arraySize;//sizeof(input) / sizeof(input[0]);
    Serial.println("msgLength: " + (String)msgLength);

    PIC16->beginTransaction(SPISettings(spiClk, MSBFIRST, SPI_MODE0));

    for (uint8_t i = 0; i < msgLength; i++)
    {
        Serial.print("Sending: ");
        Serial.println(input[i], DEC);
        PIC16->transfer(input[i]);
        delay(SPI_DELAY);
    }

    PIC16->endTransaction();

}
