// ----------------------------------------------------------------------------
// communication.cpp
//
//
// Authors:
// Joost 'MrToast' Van Brakel
// Arwen 'interBilly' Peters
//
// Dev start 09052020
// V. 0.2
// ----------------------------------------------------------------------------
#ifndef COMMUNICATION_H
#define COMMUNICATION_H
using namespace std;
#include "Driver.h"

void decodeMessage(char*, char*, unsigned char);
char* splitStrings(char* messageString, char* splitString);
uint8_t* GetDataLed(char* DataString, uint16_t* Led);
uint8_t* GetData(char* DataString);
char* GenerateReturnDataString(uint8_t* ReturnData, size_t arraylength);
void sendLedController(uint8_t type, uint16_t Led, uint8_t* Data, size_t* arraylength);
void sendPic(uint8_t type, uint8_t* Data, size_t* arraylength);
void setupCommunication(Driver* driverPointer);
void testSPI(char* input, int arraySize);
bool SPIlisten(uint8_t* returnData);
#endif
