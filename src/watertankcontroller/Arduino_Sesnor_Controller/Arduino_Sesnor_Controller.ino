#include <Wire.h>

void setup()
{
    Serial.begin(9600);

}
void loop()
{
    Serial.println(getLevel());
}

byte getLevel()
{
    byte WaterLevel = 0;
    byte Threshold = 100;
    byte Readings[20];

    Wire.requestFrom(0x77, 8);
    while (8 != Wire.available())
    {
    }
    for (int i = 0; i <= 7; i += (1))
    {
        Readings[i] = Wire.read();
    }
    Wire.requestFrom(0x78, 12);
    while (12 != Wire.available())
    {
    }
    for (int i = 8; i <= 19; i += (1))
    {
        Readings[i] = Wire.read();
    }
    for (int i = 0; i <= 19; i += (1))
    {
        if (Readings[i] > Threshold)
        {
            WaterLevel = WaterLevel + 5;
        }
    }
    return WaterLevel;
}
