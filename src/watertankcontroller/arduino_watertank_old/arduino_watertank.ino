#include <Wire.h>

double Water = 45000;
int InputWaterPerSecond = 1;
int OutputWaterPerSecond = 1;
char isInputValveOpen = 0;
char isOutputValveOpen = 0;
double WaterLevel = 0;

void setup ()
{
    // TODO: put your setup code here, to run once:
    Wire.begin(0x78);                // join i2c bus
    Wire.onRequest(requestEvent); // register event
    Wire.setClock(100000);
    Serial.begin(9600);
}

void loop()
{
    // TODO: put your main code here, to run repeatedly:
    isInputValveOpen = digitalRead(2);
    isOutputValveOpen = digitalRead(3);

    if(isInputValveOpen)
    {
        Water = Water + InputWaterPerSecond;
    }
    if(isOutputValveOpen)
    {
        Water = Water - OutputWaterPerSecond;
    }

    if(Water > 45000)
    {
        Water = 45000;
    }
    if(Water < 0)
    {
        Water = 0;
    }

    WaterLevel = (double)Water / 45000 * 100;
    Serial.println("water: " + String(WaterLevel));
    delay(1000);
}

void requestEvent()
{
    Serial.println("interrupt");
    byte i2c_addr = TWDR >> 1; // retrieve address from last byte on the bus
    switch (i2c_addr)
    {
        case 0x77:
            Serial.println("run low");
            sendLow();
            break;
        case 0x78:
            Serial.println("run high");
            Wire.write(0);
            sendHigh();
            break;
        default:
            // I2C address does not match any of our sensors', ignore.
            break;
    }
}

void sendLow()
{
    //Wire.beginTransmission(0x77);
    for(int i = 1; i <= 8; i++)
    {
        if(WaterLevel >= i*5)
        {
            Wire.write(0);
        }
        else
        {
            Wire.write(0);
        }
    }
    //Wire.endTransmission();
}

void sendHigh()
{
    Serial.println("high function");
    //Wire.beginTransmission(0x78);
    Serial.println("transmission start");
    for(int i = 9; i <= 20; i++)
    {
        if(WaterLevel >= i*5)
        {
            char* testtt = 200;
            Wire.write(testtt);
            Serial.println("test send high");
        }
        else
        {
            Wire.write(0);
            Serial.println("test send low");
        }
        Serial.println(i);
    }
    //Wire.endTransmission();
}
