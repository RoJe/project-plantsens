#include <Wire.h>

byte WaterLevel = 0;

void setup ()
{
    // TODO: put your setup code here, to run once:
    Wire.begin(0x77);                // join i2c bus
    Wire.onRequest(requestEvent); // register event
    pinMode(6, INPUT);
    pinMode(7, INPUT);
    pinMode(8, INPUT);
    pinMode(9, INPUT);
    pinMode(10, INPUT);
    pinMode(11, INPUT);
    pinMode(12, INPUT);
    pinMode(13, INPUT);
}

void loop()
{
    bitWrite(WaterLevel, 0, digitalRead(6));
    bitWrite(WaterLevel, 1, digitalRead(7));
    bitWrite(WaterLevel, 2, digitalRead(8));
    bitWrite(WaterLevel, 3, digitalRead(9));
    bitWrite(WaterLevel, 4, digitalRead(10));
    bitWrite(WaterLevel, 5, digitalRead(11));
    bitWrite(WaterLevel, 6, digitalRead(12));
    bitWrite(WaterLevel, 7, digitalRead(13));
    delay(1000);
}

void requestEvent()
{
    for(int i = 1; i <= 8; i++)
    {
        if(WaterLevel >= i*5)
        {
            Wire.write(255);
        }
        else
        {
            Wire.write(0);
        }
    }
}
