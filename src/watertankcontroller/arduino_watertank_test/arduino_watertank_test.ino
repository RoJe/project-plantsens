#include <Wire.h>

void setup ()
{
    // TODO: put your setup code here, to run once:
    Wire.begin();                // join i2c bus
    Serial.begin(9600);
    Wire.setClock(100000);
    delay(1000);
}

void loop()
{
    // TODO: put your main code here, to run repeatedly:
    Serial.println("test start");
    Wire.requestFrom(0x78, 12);
    Serial.println("test request done / waiting");
    while (12 != Wire.available());

    Serial.println("test waiting done");
    for (int i = 0; i < 8 ; i++)
    {
        Serial.println(i);
        Serial.println("read test: " + String(Wire.read())); // receive a byte as character
    }
    Serial.println("test printing done");
    delay(1000);
}
