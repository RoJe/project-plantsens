double Water = 45000;
int InputWaterPerSecond = 1;
int OutputWaterPerSecond = 1;
byte isInputValveOpen = 0;
byte isOutputValveOpen = 0;
byte WaterLevel = 0;

void setup()
{
    Serial.begin(9600);
    pinMode(6, OUTPUT);
    pinMode(7, OUTPUT);
    pinMode(8, OUTPUT);
    pinMode(9, OUTPUT);
    pinMode(10, OUTPUT);
    pinMode(11, OUTPUT);
    pinMode(12, OUTPUT);
    pinMode(13, OUTPUT);
}

void loop()
{
    isInputValveOpen = digitalRead(2);
    isOutputValveOpen = digitalRead(3);

    if(isInputValveOpen)
    {
        Water = Water + InputWaterPerSecond;
    }
    if(isOutputValveOpen)
    {
        Water = Water - OutputWaterPerSecond;
    }

    if(Water > 45000)
    {
        Water = 45000;
    }
    if(Water < 0)
    {
        Water = 0;
    }

    WaterLevel = Water / 45000 * 100;
    Serial.println("water: " + String(WaterLevel));
    digitalWrite(6,bitRead(WaterLevel,0));
    digitalWrite(7,bitRead(WaterLevel,1));
    digitalWrite(8,bitRead(WaterLevel,2));
    digitalWrite(9,bitRead(WaterLevel,3));
    digitalWrite(10,bitRead(WaterLevel,4));
    digitalWrite(11,bitRead(WaterLevel,5));
    digitalWrite(12,bitRead(WaterLevel,6));
    digitalWrite(13,bitRead(WaterLevel,7));
    delay(1000);
}
