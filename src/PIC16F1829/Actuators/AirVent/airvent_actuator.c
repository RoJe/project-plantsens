#include <stdint.h>

#include "airvent_actuator.h"
#include "../../Controller/DecoderController/decoder_channels.h"
#include "../../Controller/DecoderController/decoder_controller.h"

#include "../../Communication/spicom.h"
#include "../../Command/command_index.h"
void airvent_on(void)
{
    uint8_t reply = 1;
    decoder_set_channel(CHAN_AIRVENT);
    spi_reply(CMD_AIRFLOW_ENABLE, &reply, sizeof(reply));
}
void airvent_off(void)
{
    uint8_t reply = 1;
    if(decoder_current_channel == CHAN_AIRVENT)
    {
        decoder_set_channel(CHAN_NULL);
    }
    spi_reply(CMD_AIRFLOW_DISABLE, &reply, sizeof(reply));
}
