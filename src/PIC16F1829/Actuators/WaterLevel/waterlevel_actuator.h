#ifndef WATERLEVEL_ACTUATOR_H
#define WATERLEVEL_ACTUATOR_H

void inlet_open();
void inlet_close();

void outlet_open();
void outlet_close();

#define INLET_PIN

#endif //WATERLEVEL_ACTUATOR_H