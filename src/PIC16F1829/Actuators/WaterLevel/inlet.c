#include "waterlevel_actuator.h"
#include "../../Controller/DecoderController/decoder_channels.h"
#include "../../Controller/DecoderController/decoder_controller.h"

#include "../../Communication/spicom.h"
#include "../../Command/command_index.h"

void inlet_open()
{
    uint8_t reply = 1;
    decoder_set_channel(CHAN_WATER_INLET);
    spi_reply(CMD_WATER_INLET_VALVE_OPEN, &reply, sizeof(reply));
}
void inlet_close()
{
    uint8_t reply = 1;
    if(decoder_current_channel == CHAN_WATER_INLET)
    {
        decoder_set_channel(CHAN_NULL);
    }
    spi_reply(CMD_WATER_INLET_VALVE_OPEN, &reply, sizeof(reply));
}

