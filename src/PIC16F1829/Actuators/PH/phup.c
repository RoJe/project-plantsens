#include "ph_actuator.h"
#include "../../mcc_generated_files/pin_manager.h"
#include "../../Controller/DecoderController/decoder_channels.h"
#include "../../Controller/DecoderController/decoder_controller.h"


#include "../../Communication/spicom.h"
#include "../../Command/command_index.h"

void phup_pump_enable(void)
{
    uint8_t reply = 1;
    decoder_set_channel(CHAN_PH_UP);
    spi_reply(CMD_PH_UP_PUMP_ON, &reply, sizeof(reply));
}
void phup_pump_disable(void)
{
    uint8_t reply = 1;
    if(decoder_current_channel == CHAN_PH_UP)
    {
        decoder_set_channel(CHAN_NULL);
    }
    spi_reply(CMD_PH_UP_PUMP_ON, &reply, sizeof(reply));
}

