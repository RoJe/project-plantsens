
#ifndef PH_ACTUATOR_H
#define PH_ACTUATOR_H

void phdown_pump_enable(void);
void phdown_pump_disable(void);

void phup_pump_enable(void);
void phup_pump_disable(void);

#endif