#include "ph_actuator.h"
#include "../../mcc_generated_files/pin_manager.h"
#include "../../Controller/DecoderController/decoder_channels.h"
#include "../../Controller/DecoderController/decoder_controller.h"

#include "../../Communication/spicom.h"
#include "../../Command/command_index.h"

void phdown_pump_enable(void)
{
    uint8_t reply = 1;
    decoder_set_channel(CHAN_PH_DOWN);
    spi_reply(CMD_PH_DOWN_PUMP_ON, &reply, sizeof(reply));

}
void phdown_pump_disable(void)
{
    uint8_t reply = 1;
    if(decoder_current_channel == CHAN_PH_DOWN)
    {
        decoder_set_channel(CHAN_NULL);
    }
    spi_reply(CMD_PH_DOWN_PUMP_OFF, &reply, sizeof(reply));
}


