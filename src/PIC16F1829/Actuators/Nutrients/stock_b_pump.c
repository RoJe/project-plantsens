#include "../../mcc_generated_files/pin_manager.h"
#include "../../Controller/DecoderController/decoder_channels.h"
#include "../../Controller/DecoderController/decoder_controller.h"
#include "nutrient_actuator.h"

#include "../../Communication/spicom.h"
#include "../../Command/command_index.h"

void stock_b_pump_enable(void)
{
    uint8_t reply = 1;
    decoder_set_channel(CHAN_STOCK_B);
    spi_reply(CMD_STOCK_B_PUMP_ON, &reply, sizeof(reply));

}
void stock_b_pump_disable(void)
{
    uint8_t reply = 1;
    if(decoder_current_channel == CHAN_STOCK_B)
    {
        decoder_set_channel(CHAN_NULL);
    }
    spi_reply(CMD_STOCK_B_PUMP_ON, &reply, sizeof(reply));
}