#include "nutrient_actuator.h"
#include "../../Controller/DecoderController/decoder_channels.h"
#include "../../Controller/DecoderController/decoder_controller.h"

#include "../../Communication/spicom.h"
#include "../../Command/command_index.h"

void stock_a_pump_enable(void)
{
    uint8_t reply = 1;
    decoder_set_channel(CHAN_STOCK_A);
    spi_reply(CMD_STOCK_A_PUMP_ON, &reply, sizeof(reply));

}
void stock_a_pump_disable(void)
{
    uint8_t reply = 1;
    if(decoder_current_channel == CHAN_STOCK_B)
    {
        decoder_set_channel(CHAN_NULL);
    }
    spi_reply(CMD_STOCK_A_PUMP_OFF, &reply, sizeof(reply));
}
