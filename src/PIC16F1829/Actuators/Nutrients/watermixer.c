#include "nutrient_actuator.h"
#include "../../mcc_generated_files/pin_manager.h"

void watermixer_enable(void)
{
    WATER_MIXER_SetHigh();
}

void watermixer_disable(void)
{
    WATER_MIXER_SetLow();
}