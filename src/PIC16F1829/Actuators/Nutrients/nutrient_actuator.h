#ifndef NUTRIENT_ACTUATOR_H
#define NUTRIENT_ACTUATOR_H

void watermixer_enable(void);
void watermixer_disable(void);

void stock_a_pump_enable(void);
void stock_a_pump_disable(void);

void stock_b_pump_enable(void);
void stock_b_pump_disable(void);

#endif //NUTRIENT_ACTUATOR_H
