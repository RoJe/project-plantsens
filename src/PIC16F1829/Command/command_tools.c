#include <stdint.h>

#include "command_builder.h"

uint8_t cmd_num_args(uint8_t cmd)
/*returns the amount of byte arguments a command has*/
{
    switch(cmd)
    {
        case 0b01110010:
            return 2;
            break;
        default:
            return 0;
            break;
    }
}

uint8_t cmd_extract_command(struct cmd_t cmd)
{
    return cmd.cmd & 0x0F;
}
