/*
 * File:   command_handler.c
 * Author: paul
 *
 * Created on May 2, 2020, 5:08 PM
 */
#include <stdint.h>
#include "command_tools.h"
#include "command_builder.h"
#include "command_executor.h"
#include "../Communication/spicom.h"

typedef enum
{
    CMD_WAIT_DATA,
    CMD_RECV_CMD,
    CMD_RECV_ARGS,
    CMD_EXEC,
} cmd_recv_states;

uint8_t recv_state = CMD_WAIT_DATA;

/*be aware: this struct gets very dirty during the execution of the software
 *the clean parts are the data that is required by all commands
 * it is upto the code in the command to keep track of how many args are
 * required
 */
struct cmd_t cmd_buff;

/*the amount of bytes the command needs to wait for before execution can start*/
uint8_t num_arg_bytes;
/*remembers where to write next arg byte between calls of on_new_byte*/
uint8_t current_arg_pos;

void cmd_build_command(uint8_t byte)
/* build up a command one byte at a time.
 * If command is complete, executes the command
 */
{
    switch (recv_state)
    {
        case CMD_WAIT_DATA:
            if (byte != 0)
            {
                current_arg_pos = 0;
                recv_state = CMD_RECV_CMD;
                num_arg_bytes = byte; //set amount of bytes to be received
            }
            break;

        case CMD_RECV_CMD:
            cmd_buff.cmd = byte;
            num_arg_bytes--; //subtract one to know how many params to receive
            if (num_arg_bytes == 0)   //if no params should be received
            {
                recv_state = CMD_EXEC;
                cmd_exec(cmd_buff);

                /*reset state after command has been executed*/
                recv_state = CMD_WAIT_DATA;

            }
            else
            {
                recv_state = CMD_RECV_ARGS;
                current_arg_pos = 0;
            }
            break;
        case CMD_RECV_ARGS:
            cmd_buff.args[current_arg_pos++] = byte;
            if (current_arg_pos == num_arg_bytes)
            {
                /*wait for all bytes to have been received*/
                recv_state = CMD_EXEC;
                cmd_exec(cmd_buff);

                /*reset state after command has been executed*/
                recv_state = CMD_WAIT_DATA;
            }
            break;
        case CMD_EXEC:
            /*skip reading commands while command is already being executed*/
            break;
    }
}

