#ifndef COMMAND_EXECUTOR_H
#define COMMAND_EXECUTOR_H

#include "command_builder.h"

void cmd_exec(struct cmd_t cmd);

#endif