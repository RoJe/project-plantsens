//executes a fully received command

#include <stdint.h>

#include "command_executor.h"
#include "Catagories/cmd_catagories.h"
#include "command_builder.h"

void cmd_exec(struct cmd_t cmd)
{
    uint8_t handle_target;
    handle_target = (cmd.cmd & 0xF0) >> 4; //get 4 bytes for the target
    switch(handle_target)
    {
        case(0b0000):
            cmd_microcontroller(cmd);
            break;
        case (0b0001):
            cmd_waterlevel(cmd);
            break;
        case(0b0010):
            cmd_waterph(cmd);
            break;
        case(0b0011):
            cmd_waternutrient(cmd);
            break;
        case(0b0100):
            //cmd_watertemperature(cmd);
            break;
        case(0b0101):
            cmd_greenhouseoxygen(cmd);
            break;
        case(0b0110):
            cmd_greenhouseclimate(cmd);
            break;
        case(0b0111):
            //cmd_automationcontrol(cmd);
            break;
        default:
            break;
    }
}

