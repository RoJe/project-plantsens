#include "cmd_catagories.h"
#include "../command_builder.h"
#include "../../Controller/operation_controller.h"
#include "../../Controller/operations.h"
#include "../../Sensors/PHSensor/ph_sensor.h"

void cmd_waterph(struct cmd_t cmd)
{
    uint8_t command = cmd.cmd & 0x0F;
    switch (command)
    {
        case 0b0000: //READ PH SENSOR
            OPC_put(OP_WATERPH_SENSOR_ADD_TO_QUEUE);
            break;
        case 0b0001:
            OPC_put(OP_WATERPH_SENSOR_TEST);
            break;
        case 0b0010:
            OPC_put(OP_WATERPH_UP_ON);
            break;
        case 0b0011:
            OPC_put(OP_WATERPH_UP_OFF);
            break;
        case 0b0100:
            OPC_put(OP_WATERPH_DOWN_ON);
            break;
        case 0b0101:
            OPC_put(OP_WATERPH_DOWN_OFF);
            break;
        default:
            return;
            break;
    }
}
