#include "cmd_catagories.h"
#include "../command_builder.h"
#include "../../Controller/operation_controller.h"
#include "../../Controller/operations.h"
#include "../command_tools.h"

void cmd_microcontroller(struct cmd_t cmd)
{
    uint8_t command = cmd_extract_command(cmd);
    switch (command)
    {
        case 0b0000:
            OPC_put(OP_PIC_HEARTBEAT);
            break;
        default:
            break;
    }
}
