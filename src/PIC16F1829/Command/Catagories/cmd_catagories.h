#ifndef CMD_CATAGORIES_H
#define CMD_CATAGORIES_H

#include "../command_builder.h"

void cmd_automationcontrol(struct cmd_t cmd);
void cmd_greenhouseclimate(struct cmd_t cmd);
void cmd_greenhouseoxygen(struct cmd_t cmd);
void cmd_microcontroller(struct cmd_t cmd);
void cmd_waterlevel(struct cmd_t cmd);
void cmd_waternutrient(struct cmd_t cmd);
void cmd_waterph(struct cmd_t cmd);
void cmd_watertemperature(struct cmd_t cmd);


#endif //CMD_CATAGORIES_H