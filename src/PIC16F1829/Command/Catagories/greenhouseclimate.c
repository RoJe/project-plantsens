#include <stdint.h>

#include "cmd_catagories.h"
#include "../command_builder.h"
#include "../command_tools.h"
#include "../../Controller/operation_controller.h"
#include "../../Controller/operations.h"

void cmd_greenhouseclimate(struct cmd_t cmd)
{
    uint8_t command = cmd_extract_command(cmd);
    switch(command)
    {
        case (0b0000):
            OPC_put(OP_HUMIDITY_READ_SENSOR);
            break;
        case (0b0001):
            OPC_put(OP_HUMIDITY_TEST_SENSOR);
            break;
        default:
            break;
    }
}
