#ifndef COMMAND_TOOLS_H
#define COMMAND_TOOLS_H

#include <stdint.h>
#include "command_builder.h"

uint8_t cmd_num_args(uint8_t cmd);
uint8_t cmd_extract_command(struct cmd_t cmd);


#endif //COMMAND_TOOLS_H