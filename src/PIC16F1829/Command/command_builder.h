#ifndef COMMAND_HANDLER_H
#define COMMAND_HANDLER_H

#define LONGEST_CMD 3

#include <stdint.h>

struct cmd_t
{
    char cmd;
    char args[LONGEST_CMD];
};
void cmd_build_command(uint8_t byte);


#endif //COMMAND_HANDLER_H
