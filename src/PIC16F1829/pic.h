#ifndef PIC_H
#define PIC_H

void pic_heartbeat(void);
void pic_not_supported(void);

#endif //PIC_H