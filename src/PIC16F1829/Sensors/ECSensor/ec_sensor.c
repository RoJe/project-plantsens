#include <stdint.h>
#include <math.h>
#include <stdbool.h>
#include <xc.h>
#include <stdlib.h>
#include <limits.h>

#include "ec_sensor.h"
#include "../../Command/command_index.h"
#include "../../Controller/operation_controller.h"
#include "../../Controller/operations.h"
#include "../../Communication/spicom.h"
#include "../../mcc_generated_files/adc.h"
#include "../../Controller/ADCController/adc_controller.h"


//TODO: ADD ADC CHANNEL TO BE READ

#define EC_ADC_CHANNEL 0
uint16_t ec_sensor_value;

void ec_read_sensor(void)
/*setups up the adc to read from the ec sensor*/
{
    adc_add_channel(EC_SENSOR_CHANNEL);
    return;
}

void ec_on_sensor_read(void)
/*on interrupt this function should be called*/
{
    ec_sensor_value = ADC_GetConversionResult();
    OPC_put(OP_NUTRIENT_SEND_DATA);
}

void ec_transmit() //ONLY CALL THIS FUNCTION FROM MAIN
{
    spi_reply(CMD_EC_GET_VALUE, (uint8_t*)(&ec_sensor_value), sizeof(ec_sensor_value));
}

void ec_test_sensor()
/* tests sensor by checking consistancy in reading*/
{
    uint16_t threshold = 0b0000000111; //allowed difference between two values
    uint16_t min = 0xFFFF;
    uint16_t max = 0x0000;
    uint8_t reply;

    PIE1bits.ADIE = 0; // disable adc interrupts

    for (int i=0; i<5; i++)
    {
        uint16_t measured = ADC_GetConversion(EC_SENSOR_CHANNEL);
        if (measured < min)
        {
            min = measured;
        }
        if (measured > max)
        {
            max = measured;
        }
        if (abs(min - max) > threshold)
        {
            reply = 10;
        }
    }
    reply = 1;
    spi_reply(CMD_EC_TEST_SENSOR, &reply, sizeof(reply));

    PIE1bits.ADIE = 1;
}
