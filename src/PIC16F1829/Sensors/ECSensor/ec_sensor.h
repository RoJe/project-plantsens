#ifndef EC_SENSOR_H
#define EC_SENSOR_H

#include <stdint.h>

void ec_transmit(void);
void ec_read_sensor();
void ec_on_sensor_read();
void ec_test_sensor();


#endif