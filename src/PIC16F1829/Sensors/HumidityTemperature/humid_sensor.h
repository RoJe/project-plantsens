#ifndef HUMID_SENSOR_H
#define HUMID_SENSOR_H

void humid_read_sensor(void);
void humid_send_data(void);
void humid_send_error(void);

#endif //HUMID_SENSOR_H