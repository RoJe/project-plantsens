#include "oxygen_sensor.h"
#include "../../Communication/spicom.h"
#include "../../Communication/eusartcom.h"
#include "../../Controller/operation_controller.h"
#include "../../Controller/operations.h"

void sensor_data_ready_handler(void);
uint8_t oxygen_read_buffer(char* data);

void oxygen_sensor_init(void)
/*sets sensor to respond mode
 prepares sending and receiving buffer for uart
 eats ass
 */
{
    eusart_set_string_handler(sensor_data_ready_handler);
    eusart_transmit("M 1\r\n"); //set the sensor in polling mode
}

void oxygen_sensor_test(void)
{
    eusart_transmit("T\r\n");
}

void oxygen_read_sensor(void)
{
    eusart_transmit("%\r\n");
}

void oxygen_parse_buffer(void)
{
    char data[15]; //buffer for the command parameters
    uint8_t data_size = 1;

    //start by checking if the reponse is the all values response
    //if so clear the buffer until the next newline
    eusart_rx_pop(&data); //read the command from the spi buffer
    if (data[0] == 'A')
    {
        char tmp;
        do
        {
            eusart_rx_pop(&tmp);
        }
        while ( tmp != '\n');
    }

    //read data until next newline, don't overwrite earlier read command
    data_size += oxygen_read_buffer(data+1);
    //spi_transmit(data, data_size);
}

uint8_t oxygen_read_buffer(char* data)
{
    char tmp;
    char buff_pos = 0;
    do
    {
        eusart_rx_pop(&tmp);
        if (tmp != '\r' && tmp != '\n')
        {
            data[buff_pos++] = tmp;
        }
    }
    while (tmp != '\n');
    return buff_pos; //buff_pos is also data size

}

void sensor_data_ready_handler(void)
{
    OPC_put(OP_OXYGEN_PARSE_DATA);
}