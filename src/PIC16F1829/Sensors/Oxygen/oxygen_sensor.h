#ifndef OXYGEN_SENSOR_H
#define OXYGEN_SENSOR_H

void oxygen_sensor_init(void);
void oxygen_sensor_test(void);
void oxygen_read_sensor(void);
void oxygen_parse_buffer(void);


#endif //OXYGEN_SENSOR_H