#include <stdint.h>
#include <math.h>
#include <stdbool.h>
#include <xc.h>
#include <stdlib.h>


#include "ph_sensor.h"
#include "../../Command/command_index.h"
#include "../../Controller/operation_controller.h"
#include "../../Controller/operations.h"
#include "../../Communication/spicom.h"
#include "../../mcc_generated_files/adc.h"
#include "../../Controller/ADCController/adc_controller.h"

uint16_t ph_sensor_value;

void ph_read_sensor()
/*setups up the adc to read from the ph sensor*/
{
    adc_add_channel(PH_SENSOR_CHANNEL); //channel defined in mcc_generated_files/adc.h
}

void ph_on_sensor_read()
/*on interrupt this function should be called*/
{
    ph_sensor_value = ADC_GetConversionResult();
    OPC_put(OP_WATERPH_SEND_DATA);
}

void ph_transmit(void)
{
    spi_reply(CMD_PH_GET_VALUE, (uint8_t*)(&ph_sensor_value), sizeof(ph_sensor_value));
    //spi_transmit((char *)(&ph_sensor_value), sizeof(ph_sensor_value));
}

void ph_test_sensor()
/* tests sensor by checking consistancy in reading*/
{
    uint16_t threshold = 0b0000000111; //allowed difference between two values
    uint16_t min = 0xFFFF;
    uint16_t max = 0x0000;
    uint8_t reply;

    PIE1bits.ADIE = 0; // disable adc interrupts

    for (int i=0; i<5; i++)
    {
        short measured = ADC_GetConversion(PH_SENSOR_CHANNEL);
        if (measured < min)
        {
            min = measured;
        }
        if (measured > max)
        {
            max = measured;
        }
        if (abs(min - max) > threshold)
        {
            reply = 10;
        }
    }
    reply = 1;
    spi_reply(CMD_PH_TEST_SENSOR, &reply, sizeof(reply));

    PIE1bits.ADIE = 1;
}