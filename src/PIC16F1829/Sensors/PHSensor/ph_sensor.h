#ifndef PH_SENSOR_H
#define PH_SENSOR_H

#include <stdint.h>

void ph_transmit(void);
void ph_read_sensor();
void ph_on_sensor_read();
void ph_test_sensor();

#endif