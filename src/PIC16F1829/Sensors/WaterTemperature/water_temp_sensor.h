#ifndef WATER_TEMP_SENSOR_H
#define WATER_TEMP_SENSOR_H

void water_temp_read_sensor();
void water_temp_send_error();
void water_temp_send_data();

#endif