/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.0
        Device            :  PIC16F1829
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.10 and above
        MPLAB 	          :  MPLAB X 5.35
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries.

    Subject to your compliance with these terms, you may use Microchip software and any
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party
    license terms applicable to your use of third party software (including open source software) that
    may accompany Microchip software.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS
    FOR A PARTICULAR PURPOSE.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

/**
  Section: Included Files
*/

#include <xc.h>

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set AIRFLOW_PIN aliases
#define AIRFLOW_PIN_TRIS                 TRISAbits.TRISA1
#define AIRFLOW_PIN_LAT                  LATAbits.LATA1
#define AIRFLOW_PIN_PORT                 PORTAbits.RA1
#define AIRFLOW_PIN_WPU                  WPUAbits.WPUA1
#define AIRFLOW_PIN_ANS                  ANSELAbits.ANSA1
#define AIRFLOW_PIN_SetHigh()            do { LATAbits.LATA1 = 1; } while(0)
#define AIRFLOW_PIN_SetLow()             do { LATAbits.LATA1 = 0; } while(0)
#define AIRFLOW_PIN_Toggle()             do { LATAbits.LATA1 = ~LATAbits.LATA1; } while(0)
#define AIRFLOW_PIN_GetValue()           PORTAbits.RA1
#define AIRFLOW_PIN_SetDigitalInput()    do { TRISAbits.TRISA1 = 1; } while(0)
#define AIRFLOW_PIN_SetDigitalOutput()   do { TRISAbits.TRISA1 = 0; } while(0)
#define AIRFLOW_PIN_SetPullup()          do { WPUAbits.WPUA1 = 1; } while(0)
#define AIRFLOW_PIN_ResetPullup()        do { WPUAbits.WPUA1 = 0; } while(0)
#define AIRFLOW_PIN_SetAnalogMode()      do { ANSELAbits.ANSA1 = 1; } while(0)
#define AIRFLOW_PIN_SetDigitalMode()     do { ANSELAbits.ANSA1 = 0; } while(0)

// get/set EC_SENSOR_CHANNEL aliases
#define EC_SENSOR_CHANNEL_TRIS                 TRISAbits.TRISA2
#define EC_SENSOR_CHANNEL_LAT                  LATAbits.LATA2
#define EC_SENSOR_CHANNEL_PORT                 PORTAbits.RA2
#define EC_SENSOR_CHANNEL_WPU                  WPUAbits.WPUA2
#define EC_SENSOR_CHANNEL_ANS                  ANSELAbits.ANSA2
#define EC_SENSOR_CHANNEL_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define EC_SENSOR_CHANNEL_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define EC_SENSOR_CHANNEL_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define EC_SENSOR_CHANNEL_GetValue()           PORTAbits.RA2
#define EC_SENSOR_CHANNEL_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define EC_SENSOR_CHANNEL_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define EC_SENSOR_CHANNEL_SetPullup()          do { WPUAbits.WPUA2 = 1; } while(0)
#define EC_SENSOR_CHANNEL_ResetPullup()        do { WPUAbits.WPUA2 = 0; } while(0)
#define EC_SENSOR_CHANNEL_SetAnalogMode()      do { ANSELAbits.ANSA2 = 1; } while(0)
#define EC_SENSOR_CHANNEL_SetDigitalMode()     do { ANSELAbits.ANSA2 = 0; } while(0)

// get/set PH_SENSOR_CHANNEL aliases
#define PH_SENSOR_CHANNEL_TRIS                 TRISAbits.TRISA4
#define PH_SENSOR_CHANNEL_LAT                  LATAbits.LATA4
#define PH_SENSOR_CHANNEL_PORT                 PORTAbits.RA4
#define PH_SENSOR_CHANNEL_WPU                  WPUAbits.WPUA4
#define PH_SENSOR_CHANNEL_ANS                  ANSELAbits.ANSA4
#define PH_SENSOR_CHANNEL_SetHigh()            do { LATAbits.LATA4 = 1; } while(0)
#define PH_SENSOR_CHANNEL_SetLow()             do { LATAbits.LATA4 = 0; } while(0)
#define PH_SENSOR_CHANNEL_Toggle()             do { LATAbits.LATA4 = ~LATAbits.LATA4; } while(0)
#define PH_SENSOR_CHANNEL_GetValue()           PORTAbits.RA4
#define PH_SENSOR_CHANNEL_SetDigitalInput()    do { TRISAbits.TRISA4 = 1; } while(0)
#define PH_SENSOR_CHANNEL_SetDigitalOutput()   do { TRISAbits.TRISA4 = 0; } while(0)
#define PH_SENSOR_CHANNEL_SetPullup()          do { WPUAbits.WPUA4 = 1; } while(0)
#define PH_SENSOR_CHANNEL_ResetPullup()        do { WPUAbits.WPUA4 = 0; } while(0)
#define PH_SENSOR_CHANNEL_SetAnalogMode()      do { ANSELAbits.ANSA4 = 1; } while(0)
#define PH_SENSOR_CHANNEL_SetDigitalMode()     do { ANSELAbits.ANSA4 = 0; } while(0)

// get/set SPI_READY_PIN aliases
#define SPI_READY_PIN_TRIS                 TRISAbits.TRISA5
#define SPI_READY_PIN_LAT                  LATAbits.LATA5
#define SPI_READY_PIN_PORT                 PORTAbits.RA5
#define SPI_READY_PIN_WPU                  WPUAbits.WPUA5
#define SPI_READY_PIN_SetHigh()            do { LATAbits.LATA5 = 1; } while(0)
#define SPI_READY_PIN_SetLow()             do { LATAbits.LATA5 = 0; } while(0)
#define SPI_READY_PIN_Toggle()             do { LATAbits.LATA5 = ~LATAbits.LATA5; } while(0)
#define SPI_READY_PIN_GetValue()           PORTAbits.RA5
#define SPI_READY_PIN_SetDigitalInput()    do { TRISAbits.TRISA5 = 1; } while(0)
#define SPI_READY_PIN_SetDigitalOutput()   do { TRISAbits.TRISA5 = 0; } while(0)
#define SPI_READY_PIN_SetPullup()          do { WPUAbits.WPUA5 = 1; } while(0)
#define SPI_READY_PIN_ResetPullup()        do { WPUAbits.WPUA5 = 0; } while(0)

// get/set RB4 procedures
#define RB4_SetHigh()            do { LATBbits.LATB4 = 1; } while(0)
#define RB4_SetLow()             do { LATBbits.LATB4 = 0; } while(0)
#define RB4_Toggle()             do { LATBbits.LATB4 = ~LATBbits.LATB4; } while(0)
#define RB4_GetValue()              PORTBbits.RB4
#define RB4_SetDigitalInput()    do { TRISBbits.TRISB4 = 1; } while(0)
#define RB4_SetDigitalOutput()   do { TRISBbits.TRISB4 = 0; } while(0)
#define RB4_SetPullup()             do { WPUBbits.WPUB4 = 1; } while(0)
#define RB4_ResetPullup()           do { WPUBbits.WPUB4 = 0; } while(0)
#define RB4_SetAnalogMode()         do { ANSELBbits.ANSB4 = 1; } while(0)
#define RB4_SetDigitalMode()        do { ANSELBbits.ANSB4 = 0; } while(0)

// get/set RB5 procedures
#define RB5_SetHigh()            do { LATBbits.LATB5 = 1; } while(0)
#define RB5_SetLow()             do { LATBbits.LATB5 = 0; } while(0)
#define RB5_Toggle()             do { LATBbits.LATB5 = ~LATBbits.LATB5; } while(0)
#define RB5_GetValue()              PORTBbits.RB5
#define RB5_SetDigitalInput()    do { TRISBbits.TRISB5 = 1; } while(0)
#define RB5_SetDigitalOutput()   do { TRISBbits.TRISB5 = 0; } while(0)
#define RB5_SetPullup()             do { WPUBbits.WPUB5 = 1; } while(0)
#define RB5_ResetPullup()           do { WPUBbits.WPUB5 = 0; } while(0)
#define RB5_SetAnalogMode()         do { ANSELBbits.ANSB5 = 1; } while(0)
#define RB5_SetDigitalMode()        do { ANSELBbits.ANSB5 = 0; } while(0)

// get/set RB6 procedures
#define RB6_SetHigh()            do { LATBbits.LATB6 = 1; } while(0)
#define RB6_SetLow()             do { LATBbits.LATB6 = 0; } while(0)
#define RB6_Toggle()             do { LATBbits.LATB6 = ~LATBbits.LATB6; } while(0)
#define RB6_GetValue()              PORTBbits.RB6
#define RB6_SetDigitalInput()    do { TRISBbits.TRISB6 = 1; } while(0)
#define RB6_SetDigitalOutput()   do { TRISBbits.TRISB6 = 0; } while(0)
#define RB6_SetPullup()             do { WPUBbits.WPUB6 = 1; } while(0)
#define RB6_ResetPullup()           do { WPUBbits.WPUB6 = 0; } while(0)

// get/set RB7 procedures
#define RB7_SetHigh()            do { LATBbits.LATB7 = 1; } while(0)
#define RB7_SetLow()             do { LATBbits.LATB7 = 0; } while(0)
#define RB7_Toggle()             do { LATBbits.LATB7 = ~LATBbits.LATB7; } while(0)
#define RB7_GetValue()              PORTBbits.RB7
#define RB7_SetDigitalInput()    do { TRISBbits.TRISB7 = 1; } while(0)
#define RB7_SetDigitalOutput()   do { TRISBbits.TRISB7 = 0; } while(0)
#define RB7_SetPullup()             do { WPUBbits.WPUB7 = 1; } while(0)
#define RB7_ResetPullup()           do { WPUBbits.WPUB7 = 0; } while(0)

// get/set DECODER_CHAN_1 aliases
#define DECODER_CHAN_1_TRIS                 TRISCbits.TRISC0
#define DECODER_CHAN_1_LAT                  LATCbits.LATC0
#define DECODER_CHAN_1_PORT                 PORTCbits.RC0
#define DECODER_CHAN_1_WPU                  WPUCbits.WPUC0
#define DECODER_CHAN_1_ANS                  ANSELCbits.ANSC0
#define DECODER_CHAN_1_SetHigh()            do { LATCbits.LATC0 = 1; } while(0)
#define DECODER_CHAN_1_SetLow()             do { LATCbits.LATC0 = 0; } while(0)
#define DECODER_CHAN_1_Toggle()             do { LATCbits.LATC0 = ~LATCbits.LATC0; } while(0)
#define DECODER_CHAN_1_GetValue()           PORTCbits.RC0
#define DECODER_CHAN_1_SetDigitalInput()    do { TRISCbits.TRISC0 = 1; } while(0)
#define DECODER_CHAN_1_SetDigitalOutput()   do { TRISCbits.TRISC0 = 0; } while(0)
#define DECODER_CHAN_1_SetPullup()          do { WPUCbits.WPUC0 = 1; } while(0)
#define DECODER_CHAN_1_ResetPullup()        do { WPUCbits.WPUC0 = 0; } while(0)
#define DECODER_CHAN_1_SetAnalogMode()      do { ANSELCbits.ANSC0 = 1; } while(0)
#define DECODER_CHAN_1_SetDigitalMode()     do { ANSELCbits.ANSC0 = 0; } while(0)

// get/set DECODER_CHAN_2 aliases
#define DECODER_CHAN_2_TRIS                 TRISCbits.TRISC1
#define DECODER_CHAN_2_LAT                  LATCbits.LATC1
#define DECODER_CHAN_2_PORT                 PORTCbits.RC1
#define DECODER_CHAN_2_WPU                  WPUCbits.WPUC1
#define DECODER_CHAN_2_ANS                  ANSELCbits.ANSC1
#define DECODER_CHAN_2_SetHigh()            do { LATCbits.LATC1 = 1; } while(0)
#define DECODER_CHAN_2_SetLow()             do { LATCbits.LATC1 = 0; } while(0)
#define DECODER_CHAN_2_Toggle()             do { LATCbits.LATC1 = ~LATCbits.LATC1; } while(0)
#define DECODER_CHAN_2_GetValue()           PORTCbits.RC1
#define DECODER_CHAN_2_SetDigitalInput()    do { TRISCbits.TRISC1 = 1; } while(0)
#define DECODER_CHAN_2_SetDigitalOutput()   do { TRISCbits.TRISC1 = 0; } while(0)
#define DECODER_CHAN_2_SetPullup()          do { WPUCbits.WPUC1 = 1; } while(0)
#define DECODER_CHAN_2_ResetPullup()        do { WPUCbits.WPUC1 = 0; } while(0)
#define DECODER_CHAN_2_SetAnalogMode()      do { ANSELCbits.ANSC1 = 1; } while(0)
#define DECODER_CHAN_2_SetDigitalMode()     do { ANSELCbits.ANSC1 = 0; } while(0)

// get/set DECODER_CHAN_3 aliases
#define DECODER_CHAN_3_TRIS                 TRISCbits.TRISC2
#define DECODER_CHAN_3_LAT                  LATCbits.LATC2
#define DECODER_CHAN_3_PORT                 PORTCbits.RC2
#define DECODER_CHAN_3_WPU                  WPUCbits.WPUC2
#define DECODER_CHAN_3_ANS                  ANSELCbits.ANSC2
#define DECODER_CHAN_3_SetHigh()            do { LATCbits.LATC2 = 1; } while(0)
#define DECODER_CHAN_3_SetLow()             do { LATCbits.LATC2 = 0; } while(0)
#define DECODER_CHAN_3_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define DECODER_CHAN_3_GetValue()           PORTCbits.RC2
#define DECODER_CHAN_3_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define DECODER_CHAN_3_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)
#define DECODER_CHAN_3_SetPullup()          do { WPUCbits.WPUC2 = 1; } while(0)
#define DECODER_CHAN_3_ResetPullup()        do { WPUCbits.WPUC2 = 0; } while(0)
#define DECODER_CHAN_3_SetAnalogMode()      do { ANSELCbits.ANSC2 = 1; } while(0)
#define DECODER_CHAN_3_SetDigitalMode()     do { ANSELCbits.ANSC2 = 0; } while(0)

// get/set WATER_MIXER aliases
#define WATER_MIXER_TRIS                 TRISCbits.TRISC3
#define WATER_MIXER_LAT                  LATCbits.LATC3
#define WATER_MIXER_PORT                 PORTCbits.RC3
#define WATER_MIXER_WPU                  WPUCbits.WPUC3
#define WATER_MIXER_ANS                  ANSELCbits.ANSC3
#define WATER_MIXER_SetHigh()            do { LATCbits.LATC3 = 1; } while(0)
#define WATER_MIXER_SetLow()             do { LATCbits.LATC3 = 0; } while(0)
#define WATER_MIXER_Toggle()             do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define WATER_MIXER_GetValue()           PORTCbits.RC3
#define WATER_MIXER_SetDigitalInput()    do { TRISCbits.TRISC3 = 1; } while(0)
#define WATER_MIXER_SetDigitalOutput()   do { TRISCbits.TRISC3 = 0; } while(0)
#define WATER_MIXER_SetPullup()          do { WPUCbits.WPUC3 = 1; } while(0)
#define WATER_MIXER_ResetPullup()        do { WPUCbits.WPUC3 = 0; } while(0)
#define WATER_MIXER_SetAnalogMode()      do { ANSELCbits.ANSC3 = 1; } while(0)
#define WATER_MIXER_SetDigitalMode()     do { ANSELCbits.ANSC3 = 0; } while(0)

// get/set RC4 procedures
#define RC4_SetHigh()            do { LATCbits.LATC4 = 1; } while(0)
#define RC4_SetLow()             do { LATCbits.LATC4 = 0; } while(0)
#define RC4_Toggle()             do { LATCbits.LATC4 = ~LATCbits.LATC4; } while(0)
#define RC4_GetValue()              PORTCbits.RC4
#define RC4_SetDigitalInput()    do { TRISCbits.TRISC4 = 1; } while(0)
#define RC4_SetDigitalOutput()   do { TRISCbits.TRISC4 = 0; } while(0)
#define RC4_SetPullup()             do { WPUCbits.WPUC4 = 1; } while(0)
#define RC4_ResetPullup()           do { WPUCbits.WPUC4 = 0; } while(0)

// get/set rx_recv aliases
#define rx_recv_TRIS                 TRISCbits.TRISC5
#define rx_recv_LAT                  LATCbits.LATC5
#define rx_recv_PORT                 PORTCbits.RC5
#define rx_recv_WPU                  WPUCbits.WPUC5
#define rx_recv_SetHigh()            do { LATCbits.LATC5 = 1; } while(0)
#define rx_recv_SetLow()             do { LATCbits.LATC5 = 0; } while(0)
#define rx_recv_Toggle()             do { LATCbits.LATC5 = ~LATCbits.LATC5; } while(0)
#define rx_recv_GetValue()           PORTCbits.RC5
#define rx_recv_SetDigitalInput()    do { TRISCbits.TRISC5 = 1; } while(0)
#define rx_recv_SetDigitalOutput()   do { TRISCbits.TRISC5 = 0; } while(0)
#define rx_recv_SetPullup()          do { WPUCbits.WPUC5 = 1; } while(0)
#define rx_recv_ResetPullup()        do { WPUCbits.WPUC5 = 0; } while(0)

// get/set RC6 procedures
#define RC6_SetHigh()            do { LATCbits.LATC6 = 1; } while(0)
#define RC6_SetLow()             do { LATCbits.LATC6 = 0; } while(0)
#define RC6_Toggle()             do { LATCbits.LATC6 = ~LATCbits.LATC6; } while(0)
#define RC6_GetValue()              PORTCbits.RC6
#define RC6_SetDigitalInput()    do { TRISCbits.TRISC6 = 1; } while(0)
#define RC6_SetDigitalOutput()   do { TRISCbits.TRISC6 = 0; } while(0)
#define RC6_SetPullup()             do { WPUCbits.WPUC6 = 1; } while(0)
#define RC6_ResetPullup()           do { WPUCbits.WPUC6 = 0; } while(0)
#define RC6_SetAnalogMode()         do { ANSELCbits.ANSC6 = 1; } while(0)
#define RC6_SetDigitalMode()        do { ANSELCbits.ANSC6 = 0; } while(0)

// get/set RC7 procedures
#define RC7_SetHigh()            do { LATCbits.LATC7 = 1; } while(0)
#define RC7_SetLow()             do { LATCbits.LATC7 = 0; } while(0)
#define RC7_Toggle()             do { LATCbits.LATC7 = ~LATCbits.LATC7; } while(0)
#define RC7_GetValue()              PORTCbits.RC7
#define RC7_SetDigitalInput()    do { TRISCbits.TRISC7 = 1; } while(0)
#define RC7_SetDigitalOutput()   do { TRISCbits.TRISC7 = 0; } while(0)
#define RC7_SetPullup()             do { WPUCbits.WPUC7 = 1; } while(0)
#define RC7_ResetPullup()           do { WPUCbits.WPUC7 = 0; } while(0)
#define RC7_SetAnalogMode()         do { ANSELCbits.ANSC7 = 1; } while(0)
#define RC7_SetDigitalMode()        do { ANSELCbits.ANSC7 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/