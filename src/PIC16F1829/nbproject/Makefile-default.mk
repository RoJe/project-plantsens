#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/PIC16F1829.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/PIC16F1829.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=Actuators/AirVent/airvent_actuator.c Actuators/Nutrients/stock_b_pump.c Actuators/Nutrients/watermixer.c Actuators/Nutrients/stock_a_pump.c Actuators/PH/phdown.c Actuators/PH/phup.c Actuators/WaterLevel/inlet.c Actuators/WaterLevel/outlet.c Command/Catagories/waterlevel.c Command/Catagories/automationcontrol.c Command/Catagories/microcontroller.c Command/Catagories/waterph.c Command/Catagories/greenhouseclimate.c Command/Catagories/watertemperature.c Command/Catagories/waternutrient.c Command/Catagories/greenhouseoxygen.c Command/command_builder.c Command/command_tools.c Command/command_executor.c Communication/eusartcom.c Communication/spicom.c Controller/ADCController/adc_controller.c Controller/DecoderController/decoder_controller.c Controller/operation_controller.c Controller/operations.c helpers/i2c/i2c.c mcc_generated_files/examples/i2c2_master_example.c mcc_generated_files/spi1.c mcc_generated_files/eusart.c mcc_generated_files/adc.c mcc_generated_files/i2c2_master.c mcc_generated_files/device_config.c mcc_generated_files/interrupt_manager.c mcc_generated_files/mcc.c mcc_generated_files/pin_manager.c Sensors/ECSensor/ec_sensor.c Sensors/HumidityTemperature/humid_sensor.c Sensors/Oxygen/oxygen_sensor.c Sensors/PHSensor/ph_sensor.c Sensors/WaterLevel/waterlevel_sensor.c Sensors/WaterTemperature/water_temp_sensor.c types/queue/queue.c main.c pic.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/Actuators/AirVent/airvent_actuator.p1 ${OBJECTDIR}/Actuators/Nutrients/stock_b_pump.p1 ${OBJECTDIR}/Actuators/Nutrients/watermixer.p1 ${OBJECTDIR}/Actuators/Nutrients/stock_a_pump.p1 ${OBJECTDIR}/Actuators/PH/phdown.p1 ${OBJECTDIR}/Actuators/PH/phup.p1 ${OBJECTDIR}/Actuators/WaterLevel/inlet.p1 ${OBJECTDIR}/Actuators/WaterLevel/outlet.p1 ${OBJECTDIR}/Command/Catagories/waterlevel.p1 ${OBJECTDIR}/Command/Catagories/automationcontrol.p1 ${OBJECTDIR}/Command/Catagories/microcontroller.p1 ${OBJECTDIR}/Command/Catagories/waterph.p1 ${OBJECTDIR}/Command/Catagories/greenhouseclimate.p1 ${OBJECTDIR}/Command/Catagories/watertemperature.p1 ${OBJECTDIR}/Command/Catagories/waternutrient.p1 ${OBJECTDIR}/Command/Catagories/greenhouseoxygen.p1 ${OBJECTDIR}/Command/command_builder.p1 ${OBJECTDIR}/Command/command_tools.p1 ${OBJECTDIR}/Command/command_executor.p1 ${OBJECTDIR}/Communication/eusartcom.p1 ${OBJECTDIR}/Communication/spicom.p1 ${OBJECTDIR}/Controller/ADCController/adc_controller.p1 ${OBJECTDIR}/Controller/DecoderController/decoder_controller.p1 ${OBJECTDIR}/Controller/operation_controller.p1 ${OBJECTDIR}/Controller/operations.p1 ${OBJECTDIR}/helpers/i2c/i2c.p1 ${OBJECTDIR}/mcc_generated_files/examples/i2c2_master_example.p1 ${OBJECTDIR}/mcc_generated_files/spi1.p1 ${OBJECTDIR}/mcc_generated_files/eusart.p1 ${OBJECTDIR}/mcc_generated_files/adc.p1 ${OBJECTDIR}/mcc_generated_files/i2c2_master.p1 ${OBJECTDIR}/mcc_generated_files/device_config.p1 ${OBJECTDIR}/mcc_generated_files/interrupt_manager.p1 ${OBJECTDIR}/mcc_generated_files/mcc.p1 ${OBJECTDIR}/mcc_generated_files/pin_manager.p1 ${OBJECTDIR}/Sensors/ECSensor/ec_sensor.p1 ${OBJECTDIR}/Sensors/HumidityTemperature/humid_sensor.p1 ${OBJECTDIR}/Sensors/Oxygen/oxygen_sensor.p1 ${OBJECTDIR}/Sensors/PHSensor/ph_sensor.p1 ${OBJECTDIR}/Sensors/WaterLevel/waterlevel_sensor.p1 ${OBJECTDIR}/Sensors/WaterTemperature/water_temp_sensor.p1 ${OBJECTDIR}/types/queue/queue.p1 ${OBJECTDIR}/main.p1 ${OBJECTDIR}/pic.p1
POSSIBLE_DEPFILES=${OBJECTDIR}/Actuators/AirVent/airvent_actuator.p1.d ${OBJECTDIR}/Actuators/Nutrients/stock_b_pump.p1.d ${OBJECTDIR}/Actuators/Nutrients/watermixer.p1.d ${OBJECTDIR}/Actuators/Nutrients/stock_a_pump.p1.d ${OBJECTDIR}/Actuators/PH/phdown.p1.d ${OBJECTDIR}/Actuators/PH/phup.p1.d ${OBJECTDIR}/Actuators/WaterLevel/inlet.p1.d ${OBJECTDIR}/Actuators/WaterLevel/outlet.p1.d ${OBJECTDIR}/Command/Catagories/waterlevel.p1.d ${OBJECTDIR}/Command/Catagories/automationcontrol.p1.d ${OBJECTDIR}/Command/Catagories/microcontroller.p1.d ${OBJECTDIR}/Command/Catagories/waterph.p1.d ${OBJECTDIR}/Command/Catagories/greenhouseclimate.p1.d ${OBJECTDIR}/Command/Catagories/watertemperature.p1.d ${OBJECTDIR}/Command/Catagories/waternutrient.p1.d ${OBJECTDIR}/Command/Catagories/greenhouseoxygen.p1.d ${OBJECTDIR}/Command/command_builder.p1.d ${OBJECTDIR}/Command/command_tools.p1.d ${OBJECTDIR}/Command/command_executor.p1.d ${OBJECTDIR}/Communication/eusartcom.p1.d ${OBJECTDIR}/Communication/spicom.p1.d ${OBJECTDIR}/Controller/ADCController/adc_controller.p1.d ${OBJECTDIR}/Controller/DecoderController/decoder_controller.p1.d ${OBJECTDIR}/Controller/operation_controller.p1.d ${OBJECTDIR}/Controller/operations.p1.d ${OBJECTDIR}/helpers/i2c/i2c.p1.d ${OBJECTDIR}/mcc_generated_files/examples/i2c2_master_example.p1.d ${OBJECTDIR}/mcc_generated_files/spi1.p1.d ${OBJECTDIR}/mcc_generated_files/eusart.p1.d ${OBJECTDIR}/mcc_generated_files/adc.p1.d ${OBJECTDIR}/mcc_generated_files/i2c2_master.p1.d ${OBJECTDIR}/mcc_generated_files/device_config.p1.d ${OBJECTDIR}/mcc_generated_files/interrupt_manager.p1.d ${OBJECTDIR}/mcc_generated_files/mcc.p1.d ${OBJECTDIR}/mcc_generated_files/pin_manager.p1.d ${OBJECTDIR}/Sensors/ECSensor/ec_sensor.p1.d ${OBJECTDIR}/Sensors/HumidityTemperature/humid_sensor.p1.d ${OBJECTDIR}/Sensors/Oxygen/oxygen_sensor.p1.d ${OBJECTDIR}/Sensors/PHSensor/ph_sensor.p1.d ${OBJECTDIR}/Sensors/WaterLevel/waterlevel_sensor.p1.d ${OBJECTDIR}/Sensors/WaterTemperature/water_temp_sensor.p1.d ${OBJECTDIR}/types/queue/queue.p1.d ${OBJECTDIR}/main.p1.d ${OBJECTDIR}/pic.p1.d

# Object Files
OBJECTFILES=${OBJECTDIR}/Actuators/AirVent/airvent_actuator.p1 ${OBJECTDIR}/Actuators/Nutrients/stock_b_pump.p1 ${OBJECTDIR}/Actuators/Nutrients/watermixer.p1 ${OBJECTDIR}/Actuators/Nutrients/stock_a_pump.p1 ${OBJECTDIR}/Actuators/PH/phdown.p1 ${OBJECTDIR}/Actuators/PH/phup.p1 ${OBJECTDIR}/Actuators/WaterLevel/inlet.p1 ${OBJECTDIR}/Actuators/WaterLevel/outlet.p1 ${OBJECTDIR}/Command/Catagories/waterlevel.p1 ${OBJECTDIR}/Command/Catagories/automationcontrol.p1 ${OBJECTDIR}/Command/Catagories/microcontroller.p1 ${OBJECTDIR}/Command/Catagories/waterph.p1 ${OBJECTDIR}/Command/Catagories/greenhouseclimate.p1 ${OBJECTDIR}/Command/Catagories/watertemperature.p1 ${OBJECTDIR}/Command/Catagories/waternutrient.p1 ${OBJECTDIR}/Command/Catagories/greenhouseoxygen.p1 ${OBJECTDIR}/Command/command_builder.p1 ${OBJECTDIR}/Command/command_tools.p1 ${OBJECTDIR}/Command/command_executor.p1 ${OBJECTDIR}/Communication/eusartcom.p1 ${OBJECTDIR}/Communication/spicom.p1 ${OBJECTDIR}/Controller/ADCController/adc_controller.p1 ${OBJECTDIR}/Controller/DecoderController/decoder_controller.p1 ${OBJECTDIR}/Controller/operation_controller.p1 ${OBJECTDIR}/Controller/operations.p1 ${OBJECTDIR}/helpers/i2c/i2c.p1 ${OBJECTDIR}/mcc_generated_files/examples/i2c2_master_example.p1 ${OBJECTDIR}/mcc_generated_files/spi1.p1 ${OBJECTDIR}/mcc_generated_files/eusart.p1 ${OBJECTDIR}/mcc_generated_files/adc.p1 ${OBJECTDIR}/mcc_generated_files/i2c2_master.p1 ${OBJECTDIR}/mcc_generated_files/device_config.p1 ${OBJECTDIR}/mcc_generated_files/interrupt_manager.p1 ${OBJECTDIR}/mcc_generated_files/mcc.p1 ${OBJECTDIR}/mcc_generated_files/pin_manager.p1 ${OBJECTDIR}/Sensors/ECSensor/ec_sensor.p1 ${OBJECTDIR}/Sensors/HumidityTemperature/humid_sensor.p1 ${OBJECTDIR}/Sensors/Oxygen/oxygen_sensor.p1 ${OBJECTDIR}/Sensors/PHSensor/ph_sensor.p1 ${OBJECTDIR}/Sensors/WaterLevel/waterlevel_sensor.p1 ${OBJECTDIR}/Sensors/WaterTemperature/water_temp_sensor.p1 ${OBJECTDIR}/types/queue/queue.p1 ${OBJECTDIR}/main.p1 ${OBJECTDIR}/pic.p1

# Source Files
SOURCEFILES=Actuators/AirVent/airvent_actuator.c Actuators/Nutrients/stock_b_pump.c Actuators/Nutrients/watermixer.c Actuators/Nutrients/stock_a_pump.c Actuators/PH/phdown.c Actuators/PH/phup.c Actuators/WaterLevel/inlet.c Actuators/WaterLevel/outlet.c Command/Catagories/waterlevel.c Command/Catagories/automationcontrol.c Command/Catagories/microcontroller.c Command/Catagories/waterph.c Command/Catagories/greenhouseclimate.c Command/Catagories/watertemperature.c Command/Catagories/waternutrient.c Command/Catagories/greenhouseoxygen.c Command/command_builder.c Command/command_tools.c Command/command_executor.c Communication/eusartcom.c Communication/spicom.c Controller/ADCController/adc_controller.c Controller/DecoderController/decoder_controller.c Controller/operation_controller.c Controller/operations.c helpers/i2c/i2c.c mcc_generated_files/examples/i2c2_master_example.c mcc_generated_files/spi1.c mcc_generated_files/eusart.c mcc_generated_files/adc.c mcc_generated_files/i2c2_master.c mcc_generated_files/device_config.c mcc_generated_files/interrupt_manager.c mcc_generated_files/mcc.c mcc_generated_files/pin_manager.c Sensors/ECSensor/ec_sensor.c Sensors/HumidityTemperature/humid_sensor.c Sensors/Oxygen/oxygen_sensor.c Sensors/PHSensor/ph_sensor.c Sensors/WaterLevel/waterlevel_sensor.c Sensors/WaterTemperature/water_temp_sensor.c types/queue/queue.c main.c pic.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/PIC16F1829.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=16F1829
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/Actuators/AirVent/airvent_actuator.p1: Actuators/AirVent/airvent_actuator.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Actuators/AirVent" 
	@${RM} ${OBJECTDIR}/Actuators/AirVent/airvent_actuator.p1.d 
	@${RM} ${OBJECTDIR}/Actuators/AirVent/airvent_actuator.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Actuators/AirVent/airvent_actuator.p1 Actuators/AirVent/airvent_actuator.c 
	@-${MV} ${OBJECTDIR}/Actuators/AirVent/airvent_actuator.d ${OBJECTDIR}/Actuators/AirVent/airvent_actuator.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Actuators/AirVent/airvent_actuator.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Actuators/Nutrients/stock_b_pump.p1: Actuators/Nutrients/stock_b_pump.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Actuators/Nutrients" 
	@${RM} ${OBJECTDIR}/Actuators/Nutrients/stock_b_pump.p1.d 
	@${RM} ${OBJECTDIR}/Actuators/Nutrients/stock_b_pump.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Actuators/Nutrients/stock_b_pump.p1 Actuators/Nutrients/stock_b_pump.c 
	@-${MV} ${OBJECTDIR}/Actuators/Nutrients/stock_b_pump.d ${OBJECTDIR}/Actuators/Nutrients/stock_b_pump.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Actuators/Nutrients/stock_b_pump.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Actuators/Nutrients/watermixer.p1: Actuators/Nutrients/watermixer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Actuators/Nutrients" 
	@${RM} ${OBJECTDIR}/Actuators/Nutrients/watermixer.p1.d 
	@${RM} ${OBJECTDIR}/Actuators/Nutrients/watermixer.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Actuators/Nutrients/watermixer.p1 Actuators/Nutrients/watermixer.c 
	@-${MV} ${OBJECTDIR}/Actuators/Nutrients/watermixer.d ${OBJECTDIR}/Actuators/Nutrients/watermixer.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Actuators/Nutrients/watermixer.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Actuators/Nutrients/stock_a_pump.p1: Actuators/Nutrients/stock_a_pump.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Actuators/Nutrients" 
	@${RM} ${OBJECTDIR}/Actuators/Nutrients/stock_a_pump.p1.d 
	@${RM} ${OBJECTDIR}/Actuators/Nutrients/stock_a_pump.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Actuators/Nutrients/stock_a_pump.p1 Actuators/Nutrients/stock_a_pump.c 
	@-${MV} ${OBJECTDIR}/Actuators/Nutrients/stock_a_pump.d ${OBJECTDIR}/Actuators/Nutrients/stock_a_pump.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Actuators/Nutrients/stock_a_pump.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Actuators/PH/phdown.p1: Actuators/PH/phdown.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Actuators/PH" 
	@${RM} ${OBJECTDIR}/Actuators/PH/phdown.p1.d 
	@${RM} ${OBJECTDIR}/Actuators/PH/phdown.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Actuators/PH/phdown.p1 Actuators/PH/phdown.c 
	@-${MV} ${OBJECTDIR}/Actuators/PH/phdown.d ${OBJECTDIR}/Actuators/PH/phdown.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Actuators/PH/phdown.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Actuators/PH/phup.p1: Actuators/PH/phup.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Actuators/PH" 
	@${RM} ${OBJECTDIR}/Actuators/PH/phup.p1.d 
	@${RM} ${OBJECTDIR}/Actuators/PH/phup.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Actuators/PH/phup.p1 Actuators/PH/phup.c 
	@-${MV} ${OBJECTDIR}/Actuators/PH/phup.d ${OBJECTDIR}/Actuators/PH/phup.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Actuators/PH/phup.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Actuators/WaterLevel/inlet.p1: Actuators/WaterLevel/inlet.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Actuators/WaterLevel" 
	@${RM} ${OBJECTDIR}/Actuators/WaterLevel/inlet.p1.d 
	@${RM} ${OBJECTDIR}/Actuators/WaterLevel/inlet.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Actuators/WaterLevel/inlet.p1 Actuators/WaterLevel/inlet.c 
	@-${MV} ${OBJECTDIR}/Actuators/WaterLevel/inlet.d ${OBJECTDIR}/Actuators/WaterLevel/inlet.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Actuators/WaterLevel/inlet.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Actuators/WaterLevel/outlet.p1: Actuators/WaterLevel/outlet.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Actuators/WaterLevel" 
	@${RM} ${OBJECTDIR}/Actuators/WaterLevel/outlet.p1.d 
	@${RM} ${OBJECTDIR}/Actuators/WaterLevel/outlet.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Actuators/WaterLevel/outlet.p1 Actuators/WaterLevel/outlet.c 
	@-${MV} ${OBJECTDIR}/Actuators/WaterLevel/outlet.d ${OBJECTDIR}/Actuators/WaterLevel/outlet.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Actuators/WaterLevel/outlet.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Command/Catagories/waterlevel.p1: Command/Catagories/waterlevel.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Command/Catagories" 
	@${RM} ${OBJECTDIR}/Command/Catagories/waterlevel.p1.d 
	@${RM} ${OBJECTDIR}/Command/Catagories/waterlevel.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Command/Catagories/waterlevel.p1 Command/Catagories/waterlevel.c 
	@-${MV} ${OBJECTDIR}/Command/Catagories/waterlevel.d ${OBJECTDIR}/Command/Catagories/waterlevel.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Command/Catagories/waterlevel.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Command/Catagories/automationcontrol.p1: Command/Catagories/automationcontrol.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Command/Catagories" 
	@${RM} ${OBJECTDIR}/Command/Catagories/automationcontrol.p1.d 
	@${RM} ${OBJECTDIR}/Command/Catagories/automationcontrol.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Command/Catagories/automationcontrol.p1 Command/Catagories/automationcontrol.c 
	@-${MV} ${OBJECTDIR}/Command/Catagories/automationcontrol.d ${OBJECTDIR}/Command/Catagories/automationcontrol.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Command/Catagories/automationcontrol.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Command/Catagories/microcontroller.p1: Command/Catagories/microcontroller.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Command/Catagories" 
	@${RM} ${OBJECTDIR}/Command/Catagories/microcontroller.p1.d 
	@${RM} ${OBJECTDIR}/Command/Catagories/microcontroller.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Command/Catagories/microcontroller.p1 Command/Catagories/microcontroller.c 
	@-${MV} ${OBJECTDIR}/Command/Catagories/microcontroller.d ${OBJECTDIR}/Command/Catagories/microcontroller.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Command/Catagories/microcontroller.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Command/Catagories/waterph.p1: Command/Catagories/waterph.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Command/Catagories" 
	@${RM} ${OBJECTDIR}/Command/Catagories/waterph.p1.d 
	@${RM} ${OBJECTDIR}/Command/Catagories/waterph.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Command/Catagories/waterph.p1 Command/Catagories/waterph.c 
	@-${MV} ${OBJECTDIR}/Command/Catagories/waterph.d ${OBJECTDIR}/Command/Catagories/waterph.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Command/Catagories/waterph.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Command/Catagories/greenhouseclimate.p1: Command/Catagories/greenhouseclimate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Command/Catagories" 
	@${RM} ${OBJECTDIR}/Command/Catagories/greenhouseclimate.p1.d 
	@${RM} ${OBJECTDIR}/Command/Catagories/greenhouseclimate.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Command/Catagories/greenhouseclimate.p1 Command/Catagories/greenhouseclimate.c 
	@-${MV} ${OBJECTDIR}/Command/Catagories/greenhouseclimate.d ${OBJECTDIR}/Command/Catagories/greenhouseclimate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Command/Catagories/greenhouseclimate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Command/Catagories/watertemperature.p1: Command/Catagories/watertemperature.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Command/Catagories" 
	@${RM} ${OBJECTDIR}/Command/Catagories/watertemperature.p1.d 
	@${RM} ${OBJECTDIR}/Command/Catagories/watertemperature.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Command/Catagories/watertemperature.p1 Command/Catagories/watertemperature.c 
	@-${MV} ${OBJECTDIR}/Command/Catagories/watertemperature.d ${OBJECTDIR}/Command/Catagories/watertemperature.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Command/Catagories/watertemperature.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Command/Catagories/waternutrient.p1: Command/Catagories/waternutrient.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Command/Catagories" 
	@${RM} ${OBJECTDIR}/Command/Catagories/waternutrient.p1.d 
	@${RM} ${OBJECTDIR}/Command/Catagories/waternutrient.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Command/Catagories/waternutrient.p1 Command/Catagories/waternutrient.c 
	@-${MV} ${OBJECTDIR}/Command/Catagories/waternutrient.d ${OBJECTDIR}/Command/Catagories/waternutrient.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Command/Catagories/waternutrient.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Command/Catagories/greenhouseoxygen.p1: Command/Catagories/greenhouseoxygen.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Command/Catagories" 
	@${RM} ${OBJECTDIR}/Command/Catagories/greenhouseoxygen.p1.d 
	@${RM} ${OBJECTDIR}/Command/Catagories/greenhouseoxygen.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Command/Catagories/greenhouseoxygen.p1 Command/Catagories/greenhouseoxygen.c 
	@-${MV} ${OBJECTDIR}/Command/Catagories/greenhouseoxygen.d ${OBJECTDIR}/Command/Catagories/greenhouseoxygen.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Command/Catagories/greenhouseoxygen.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Command/command_builder.p1: Command/command_builder.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Command" 
	@${RM} ${OBJECTDIR}/Command/command_builder.p1.d 
	@${RM} ${OBJECTDIR}/Command/command_builder.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Command/command_builder.p1 Command/command_builder.c 
	@-${MV} ${OBJECTDIR}/Command/command_builder.d ${OBJECTDIR}/Command/command_builder.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Command/command_builder.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Command/command_tools.p1: Command/command_tools.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Command" 
	@${RM} ${OBJECTDIR}/Command/command_tools.p1.d 
	@${RM} ${OBJECTDIR}/Command/command_tools.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Command/command_tools.p1 Command/command_tools.c 
	@-${MV} ${OBJECTDIR}/Command/command_tools.d ${OBJECTDIR}/Command/command_tools.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Command/command_tools.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Command/command_executor.p1: Command/command_executor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Command" 
	@${RM} ${OBJECTDIR}/Command/command_executor.p1.d 
	@${RM} ${OBJECTDIR}/Command/command_executor.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Command/command_executor.p1 Command/command_executor.c 
	@-${MV} ${OBJECTDIR}/Command/command_executor.d ${OBJECTDIR}/Command/command_executor.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Command/command_executor.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Communication/eusartcom.p1: Communication/eusartcom.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Communication" 
	@${RM} ${OBJECTDIR}/Communication/eusartcom.p1.d 
	@${RM} ${OBJECTDIR}/Communication/eusartcom.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Communication/eusartcom.p1 Communication/eusartcom.c 
	@-${MV} ${OBJECTDIR}/Communication/eusartcom.d ${OBJECTDIR}/Communication/eusartcom.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Communication/eusartcom.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Communication/spicom.p1: Communication/spicom.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Communication" 
	@${RM} ${OBJECTDIR}/Communication/spicom.p1.d 
	@${RM} ${OBJECTDIR}/Communication/spicom.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Communication/spicom.p1 Communication/spicom.c 
	@-${MV} ${OBJECTDIR}/Communication/spicom.d ${OBJECTDIR}/Communication/spicom.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Communication/spicom.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Controller/ADCController/adc_controller.p1: Controller/ADCController/adc_controller.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Controller/ADCController" 
	@${RM} ${OBJECTDIR}/Controller/ADCController/adc_controller.p1.d 
	@${RM} ${OBJECTDIR}/Controller/ADCController/adc_controller.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Controller/ADCController/adc_controller.p1 Controller/ADCController/adc_controller.c 
	@-${MV} ${OBJECTDIR}/Controller/ADCController/adc_controller.d ${OBJECTDIR}/Controller/ADCController/adc_controller.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Controller/ADCController/adc_controller.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Controller/DecoderController/decoder_controller.p1: Controller/DecoderController/decoder_controller.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Controller/DecoderController" 
	@${RM} ${OBJECTDIR}/Controller/DecoderController/decoder_controller.p1.d 
	@${RM} ${OBJECTDIR}/Controller/DecoderController/decoder_controller.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Controller/DecoderController/decoder_controller.p1 Controller/DecoderController/decoder_controller.c 
	@-${MV} ${OBJECTDIR}/Controller/DecoderController/decoder_controller.d ${OBJECTDIR}/Controller/DecoderController/decoder_controller.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Controller/DecoderController/decoder_controller.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Controller/operation_controller.p1: Controller/operation_controller.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Controller" 
	@${RM} ${OBJECTDIR}/Controller/operation_controller.p1.d 
	@${RM} ${OBJECTDIR}/Controller/operation_controller.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Controller/operation_controller.p1 Controller/operation_controller.c 
	@-${MV} ${OBJECTDIR}/Controller/operation_controller.d ${OBJECTDIR}/Controller/operation_controller.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Controller/operation_controller.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Controller/operations.p1: Controller/operations.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Controller" 
	@${RM} ${OBJECTDIR}/Controller/operations.p1.d 
	@${RM} ${OBJECTDIR}/Controller/operations.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Controller/operations.p1 Controller/operations.c 
	@-${MV} ${OBJECTDIR}/Controller/operations.d ${OBJECTDIR}/Controller/operations.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Controller/operations.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/helpers/i2c/i2c.p1: helpers/i2c/i2c.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/helpers/i2c" 
	@${RM} ${OBJECTDIR}/helpers/i2c/i2c.p1.d 
	@${RM} ${OBJECTDIR}/helpers/i2c/i2c.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/helpers/i2c/i2c.p1 helpers/i2c/i2c.c 
	@-${MV} ${OBJECTDIR}/helpers/i2c/i2c.d ${OBJECTDIR}/helpers/i2c/i2c.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/helpers/i2c/i2c.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/mcc_generated_files/examples/i2c2_master_example.p1: mcc_generated_files/examples/i2c2_master_example.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files/examples" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/examples/i2c2_master_example.p1.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/examples/i2c2_master_example.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/mcc_generated_files/examples/i2c2_master_example.p1 mcc_generated_files/examples/i2c2_master_example.c 
	@-${MV} ${OBJECTDIR}/mcc_generated_files/examples/i2c2_master_example.d ${OBJECTDIR}/mcc_generated_files/examples/i2c2_master_example.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/mcc_generated_files/examples/i2c2_master_example.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/mcc_generated_files/spi1.p1: mcc_generated_files/spi1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/spi1.p1.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/spi1.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/mcc_generated_files/spi1.p1 mcc_generated_files/spi1.c 
	@-${MV} ${OBJECTDIR}/mcc_generated_files/spi1.d ${OBJECTDIR}/mcc_generated_files/spi1.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/mcc_generated_files/spi1.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/mcc_generated_files/eusart.p1: mcc_generated_files/eusart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/eusart.p1.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/eusart.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/mcc_generated_files/eusart.p1 mcc_generated_files/eusart.c 
	@-${MV} ${OBJECTDIR}/mcc_generated_files/eusart.d ${OBJECTDIR}/mcc_generated_files/eusart.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/mcc_generated_files/eusart.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/mcc_generated_files/adc.p1: mcc_generated_files/adc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/adc.p1.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/adc.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/mcc_generated_files/adc.p1 mcc_generated_files/adc.c 
	@-${MV} ${OBJECTDIR}/mcc_generated_files/adc.d ${OBJECTDIR}/mcc_generated_files/adc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/mcc_generated_files/adc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/mcc_generated_files/i2c2_master.p1: mcc_generated_files/i2c2_master.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/i2c2_master.p1.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/i2c2_master.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/mcc_generated_files/i2c2_master.p1 mcc_generated_files/i2c2_master.c 
	@-${MV} ${OBJECTDIR}/mcc_generated_files/i2c2_master.d ${OBJECTDIR}/mcc_generated_files/i2c2_master.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/mcc_generated_files/i2c2_master.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/mcc_generated_files/device_config.p1: mcc_generated_files/device_config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/device_config.p1.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/device_config.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/mcc_generated_files/device_config.p1 mcc_generated_files/device_config.c 
	@-${MV} ${OBJECTDIR}/mcc_generated_files/device_config.d ${OBJECTDIR}/mcc_generated_files/device_config.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/mcc_generated_files/device_config.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/mcc_generated_files/interrupt_manager.p1: mcc_generated_files/interrupt_manager.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/interrupt_manager.p1.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/interrupt_manager.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/mcc_generated_files/interrupt_manager.p1 mcc_generated_files/interrupt_manager.c 
	@-${MV} ${OBJECTDIR}/mcc_generated_files/interrupt_manager.d ${OBJECTDIR}/mcc_generated_files/interrupt_manager.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/mcc_generated_files/interrupt_manager.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/mcc_generated_files/mcc.p1: mcc_generated_files/mcc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/mcc.p1.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/mcc.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/mcc_generated_files/mcc.p1 mcc_generated_files/mcc.c 
	@-${MV} ${OBJECTDIR}/mcc_generated_files/mcc.d ${OBJECTDIR}/mcc_generated_files/mcc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/mcc_generated_files/mcc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/mcc_generated_files/pin_manager.p1: mcc_generated_files/pin_manager.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/pin_manager.p1.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/pin_manager.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/mcc_generated_files/pin_manager.p1 mcc_generated_files/pin_manager.c 
	@-${MV} ${OBJECTDIR}/mcc_generated_files/pin_manager.d ${OBJECTDIR}/mcc_generated_files/pin_manager.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/mcc_generated_files/pin_manager.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Sensors/ECSensor/ec_sensor.p1: Sensors/ECSensor/ec_sensor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Sensors/ECSensor" 
	@${RM} ${OBJECTDIR}/Sensors/ECSensor/ec_sensor.p1.d 
	@${RM} ${OBJECTDIR}/Sensors/ECSensor/ec_sensor.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Sensors/ECSensor/ec_sensor.p1 Sensors/ECSensor/ec_sensor.c 
	@-${MV} ${OBJECTDIR}/Sensors/ECSensor/ec_sensor.d ${OBJECTDIR}/Sensors/ECSensor/ec_sensor.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Sensors/ECSensor/ec_sensor.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Sensors/HumidityTemperature/humid_sensor.p1: Sensors/HumidityTemperature/humid_sensor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Sensors/HumidityTemperature" 
	@${RM} ${OBJECTDIR}/Sensors/HumidityTemperature/humid_sensor.p1.d 
	@${RM} ${OBJECTDIR}/Sensors/HumidityTemperature/humid_sensor.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Sensors/HumidityTemperature/humid_sensor.p1 Sensors/HumidityTemperature/humid_sensor.c 
	@-${MV} ${OBJECTDIR}/Sensors/HumidityTemperature/humid_sensor.d ${OBJECTDIR}/Sensors/HumidityTemperature/humid_sensor.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Sensors/HumidityTemperature/humid_sensor.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Sensors/Oxygen/oxygen_sensor.p1: Sensors/Oxygen/oxygen_sensor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Sensors/Oxygen" 
	@${RM} ${OBJECTDIR}/Sensors/Oxygen/oxygen_sensor.p1.d 
	@${RM} ${OBJECTDIR}/Sensors/Oxygen/oxygen_sensor.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Sensors/Oxygen/oxygen_sensor.p1 Sensors/Oxygen/oxygen_sensor.c 
	@-${MV} ${OBJECTDIR}/Sensors/Oxygen/oxygen_sensor.d ${OBJECTDIR}/Sensors/Oxygen/oxygen_sensor.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Sensors/Oxygen/oxygen_sensor.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Sensors/PHSensor/ph_sensor.p1: Sensors/PHSensor/ph_sensor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Sensors/PHSensor" 
	@${RM} ${OBJECTDIR}/Sensors/PHSensor/ph_sensor.p1.d 
	@${RM} ${OBJECTDIR}/Sensors/PHSensor/ph_sensor.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Sensors/PHSensor/ph_sensor.p1 Sensors/PHSensor/ph_sensor.c 
	@-${MV} ${OBJECTDIR}/Sensors/PHSensor/ph_sensor.d ${OBJECTDIR}/Sensors/PHSensor/ph_sensor.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Sensors/PHSensor/ph_sensor.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Sensors/WaterLevel/waterlevel_sensor.p1: Sensors/WaterLevel/waterlevel_sensor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Sensors/WaterLevel" 
	@${RM} ${OBJECTDIR}/Sensors/WaterLevel/waterlevel_sensor.p1.d 
	@${RM} ${OBJECTDIR}/Sensors/WaterLevel/waterlevel_sensor.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Sensors/WaterLevel/waterlevel_sensor.p1 Sensors/WaterLevel/waterlevel_sensor.c 
	@-${MV} ${OBJECTDIR}/Sensors/WaterLevel/waterlevel_sensor.d ${OBJECTDIR}/Sensors/WaterLevel/waterlevel_sensor.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Sensors/WaterLevel/waterlevel_sensor.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Sensors/WaterTemperature/water_temp_sensor.p1: Sensors/WaterTemperature/water_temp_sensor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Sensors/WaterTemperature" 
	@${RM} ${OBJECTDIR}/Sensors/WaterTemperature/water_temp_sensor.p1.d 
	@${RM} ${OBJECTDIR}/Sensors/WaterTemperature/water_temp_sensor.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Sensors/WaterTemperature/water_temp_sensor.p1 Sensors/WaterTemperature/water_temp_sensor.c 
	@-${MV} ${OBJECTDIR}/Sensors/WaterTemperature/water_temp_sensor.d ${OBJECTDIR}/Sensors/WaterTemperature/water_temp_sensor.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Sensors/WaterTemperature/water_temp_sensor.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/types/queue/queue.p1: types/queue/queue.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/types/queue" 
	@${RM} ${OBJECTDIR}/types/queue/queue.p1.d 
	@${RM} ${OBJECTDIR}/types/queue/queue.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/types/queue/queue.p1 types/queue/queue.c 
	@-${MV} ${OBJECTDIR}/types/queue/queue.d ${OBJECTDIR}/types/queue/queue.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/types/queue/queue.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/main.p1: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.p1.d 
	@${RM} ${OBJECTDIR}/main.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/main.p1 main.c 
	@-${MV} ${OBJECTDIR}/main.d ${OBJECTDIR}/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/pic.p1: pic.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/pic.p1.d 
	@${RM} ${OBJECTDIR}/pic.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/pic.p1 pic.c 
	@-${MV} ${OBJECTDIR}/pic.d ${OBJECTDIR}/pic.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/pic.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
else
${OBJECTDIR}/Actuators/AirVent/airvent_actuator.p1: Actuators/AirVent/airvent_actuator.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Actuators/AirVent" 
	@${RM} ${OBJECTDIR}/Actuators/AirVent/airvent_actuator.p1.d 
	@${RM} ${OBJECTDIR}/Actuators/AirVent/airvent_actuator.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Actuators/AirVent/airvent_actuator.p1 Actuators/AirVent/airvent_actuator.c 
	@-${MV} ${OBJECTDIR}/Actuators/AirVent/airvent_actuator.d ${OBJECTDIR}/Actuators/AirVent/airvent_actuator.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Actuators/AirVent/airvent_actuator.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Actuators/Nutrients/stock_b_pump.p1: Actuators/Nutrients/stock_b_pump.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Actuators/Nutrients" 
	@${RM} ${OBJECTDIR}/Actuators/Nutrients/stock_b_pump.p1.d 
	@${RM} ${OBJECTDIR}/Actuators/Nutrients/stock_b_pump.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Actuators/Nutrients/stock_b_pump.p1 Actuators/Nutrients/stock_b_pump.c 
	@-${MV} ${OBJECTDIR}/Actuators/Nutrients/stock_b_pump.d ${OBJECTDIR}/Actuators/Nutrients/stock_b_pump.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Actuators/Nutrients/stock_b_pump.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Actuators/Nutrients/watermixer.p1: Actuators/Nutrients/watermixer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Actuators/Nutrients" 
	@${RM} ${OBJECTDIR}/Actuators/Nutrients/watermixer.p1.d 
	@${RM} ${OBJECTDIR}/Actuators/Nutrients/watermixer.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Actuators/Nutrients/watermixer.p1 Actuators/Nutrients/watermixer.c 
	@-${MV} ${OBJECTDIR}/Actuators/Nutrients/watermixer.d ${OBJECTDIR}/Actuators/Nutrients/watermixer.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Actuators/Nutrients/watermixer.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Actuators/Nutrients/stock_a_pump.p1: Actuators/Nutrients/stock_a_pump.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Actuators/Nutrients" 
	@${RM} ${OBJECTDIR}/Actuators/Nutrients/stock_a_pump.p1.d 
	@${RM} ${OBJECTDIR}/Actuators/Nutrients/stock_a_pump.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Actuators/Nutrients/stock_a_pump.p1 Actuators/Nutrients/stock_a_pump.c 
	@-${MV} ${OBJECTDIR}/Actuators/Nutrients/stock_a_pump.d ${OBJECTDIR}/Actuators/Nutrients/stock_a_pump.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Actuators/Nutrients/stock_a_pump.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Actuators/PH/phdown.p1: Actuators/PH/phdown.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Actuators/PH" 
	@${RM} ${OBJECTDIR}/Actuators/PH/phdown.p1.d 
	@${RM} ${OBJECTDIR}/Actuators/PH/phdown.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Actuators/PH/phdown.p1 Actuators/PH/phdown.c 
	@-${MV} ${OBJECTDIR}/Actuators/PH/phdown.d ${OBJECTDIR}/Actuators/PH/phdown.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Actuators/PH/phdown.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Actuators/PH/phup.p1: Actuators/PH/phup.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Actuators/PH" 
	@${RM} ${OBJECTDIR}/Actuators/PH/phup.p1.d 
	@${RM} ${OBJECTDIR}/Actuators/PH/phup.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Actuators/PH/phup.p1 Actuators/PH/phup.c 
	@-${MV} ${OBJECTDIR}/Actuators/PH/phup.d ${OBJECTDIR}/Actuators/PH/phup.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Actuators/PH/phup.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Actuators/WaterLevel/inlet.p1: Actuators/WaterLevel/inlet.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Actuators/WaterLevel" 
	@${RM} ${OBJECTDIR}/Actuators/WaterLevel/inlet.p1.d 
	@${RM} ${OBJECTDIR}/Actuators/WaterLevel/inlet.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Actuators/WaterLevel/inlet.p1 Actuators/WaterLevel/inlet.c 
	@-${MV} ${OBJECTDIR}/Actuators/WaterLevel/inlet.d ${OBJECTDIR}/Actuators/WaterLevel/inlet.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Actuators/WaterLevel/inlet.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Actuators/WaterLevel/outlet.p1: Actuators/WaterLevel/outlet.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Actuators/WaterLevel" 
	@${RM} ${OBJECTDIR}/Actuators/WaterLevel/outlet.p1.d 
	@${RM} ${OBJECTDIR}/Actuators/WaterLevel/outlet.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Actuators/WaterLevel/outlet.p1 Actuators/WaterLevel/outlet.c 
	@-${MV} ${OBJECTDIR}/Actuators/WaterLevel/outlet.d ${OBJECTDIR}/Actuators/WaterLevel/outlet.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Actuators/WaterLevel/outlet.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Command/Catagories/waterlevel.p1: Command/Catagories/waterlevel.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Command/Catagories" 
	@${RM} ${OBJECTDIR}/Command/Catagories/waterlevel.p1.d 
	@${RM} ${OBJECTDIR}/Command/Catagories/waterlevel.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Command/Catagories/waterlevel.p1 Command/Catagories/waterlevel.c 
	@-${MV} ${OBJECTDIR}/Command/Catagories/waterlevel.d ${OBJECTDIR}/Command/Catagories/waterlevel.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Command/Catagories/waterlevel.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Command/Catagories/automationcontrol.p1: Command/Catagories/automationcontrol.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Command/Catagories" 
	@${RM} ${OBJECTDIR}/Command/Catagories/automationcontrol.p1.d 
	@${RM} ${OBJECTDIR}/Command/Catagories/automationcontrol.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Command/Catagories/automationcontrol.p1 Command/Catagories/automationcontrol.c 
	@-${MV} ${OBJECTDIR}/Command/Catagories/automationcontrol.d ${OBJECTDIR}/Command/Catagories/automationcontrol.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Command/Catagories/automationcontrol.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Command/Catagories/microcontroller.p1: Command/Catagories/microcontroller.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Command/Catagories" 
	@${RM} ${OBJECTDIR}/Command/Catagories/microcontroller.p1.d 
	@${RM} ${OBJECTDIR}/Command/Catagories/microcontroller.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Command/Catagories/microcontroller.p1 Command/Catagories/microcontroller.c 
	@-${MV} ${OBJECTDIR}/Command/Catagories/microcontroller.d ${OBJECTDIR}/Command/Catagories/microcontroller.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Command/Catagories/microcontroller.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Command/Catagories/waterph.p1: Command/Catagories/waterph.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Command/Catagories" 
	@${RM} ${OBJECTDIR}/Command/Catagories/waterph.p1.d 
	@${RM} ${OBJECTDIR}/Command/Catagories/waterph.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Command/Catagories/waterph.p1 Command/Catagories/waterph.c 
	@-${MV} ${OBJECTDIR}/Command/Catagories/waterph.d ${OBJECTDIR}/Command/Catagories/waterph.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Command/Catagories/waterph.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Command/Catagories/greenhouseclimate.p1: Command/Catagories/greenhouseclimate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Command/Catagories" 
	@${RM} ${OBJECTDIR}/Command/Catagories/greenhouseclimate.p1.d 
	@${RM} ${OBJECTDIR}/Command/Catagories/greenhouseclimate.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Command/Catagories/greenhouseclimate.p1 Command/Catagories/greenhouseclimate.c 
	@-${MV} ${OBJECTDIR}/Command/Catagories/greenhouseclimate.d ${OBJECTDIR}/Command/Catagories/greenhouseclimate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Command/Catagories/greenhouseclimate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Command/Catagories/watertemperature.p1: Command/Catagories/watertemperature.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Command/Catagories" 
	@${RM} ${OBJECTDIR}/Command/Catagories/watertemperature.p1.d 
	@${RM} ${OBJECTDIR}/Command/Catagories/watertemperature.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Command/Catagories/watertemperature.p1 Command/Catagories/watertemperature.c 
	@-${MV} ${OBJECTDIR}/Command/Catagories/watertemperature.d ${OBJECTDIR}/Command/Catagories/watertemperature.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Command/Catagories/watertemperature.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Command/Catagories/waternutrient.p1: Command/Catagories/waternutrient.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Command/Catagories" 
	@${RM} ${OBJECTDIR}/Command/Catagories/waternutrient.p1.d 
	@${RM} ${OBJECTDIR}/Command/Catagories/waternutrient.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Command/Catagories/waternutrient.p1 Command/Catagories/waternutrient.c 
	@-${MV} ${OBJECTDIR}/Command/Catagories/waternutrient.d ${OBJECTDIR}/Command/Catagories/waternutrient.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Command/Catagories/waternutrient.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Command/Catagories/greenhouseoxygen.p1: Command/Catagories/greenhouseoxygen.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Command/Catagories" 
	@${RM} ${OBJECTDIR}/Command/Catagories/greenhouseoxygen.p1.d 
	@${RM} ${OBJECTDIR}/Command/Catagories/greenhouseoxygen.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Command/Catagories/greenhouseoxygen.p1 Command/Catagories/greenhouseoxygen.c 
	@-${MV} ${OBJECTDIR}/Command/Catagories/greenhouseoxygen.d ${OBJECTDIR}/Command/Catagories/greenhouseoxygen.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Command/Catagories/greenhouseoxygen.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Command/command_builder.p1: Command/command_builder.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Command" 
	@${RM} ${OBJECTDIR}/Command/command_builder.p1.d 
	@${RM} ${OBJECTDIR}/Command/command_builder.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Command/command_builder.p1 Command/command_builder.c 
	@-${MV} ${OBJECTDIR}/Command/command_builder.d ${OBJECTDIR}/Command/command_builder.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Command/command_builder.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Command/command_tools.p1: Command/command_tools.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Command" 
	@${RM} ${OBJECTDIR}/Command/command_tools.p1.d 
	@${RM} ${OBJECTDIR}/Command/command_tools.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Command/command_tools.p1 Command/command_tools.c 
	@-${MV} ${OBJECTDIR}/Command/command_tools.d ${OBJECTDIR}/Command/command_tools.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Command/command_tools.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Command/command_executor.p1: Command/command_executor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Command" 
	@${RM} ${OBJECTDIR}/Command/command_executor.p1.d 
	@${RM} ${OBJECTDIR}/Command/command_executor.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Command/command_executor.p1 Command/command_executor.c 
	@-${MV} ${OBJECTDIR}/Command/command_executor.d ${OBJECTDIR}/Command/command_executor.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Command/command_executor.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Communication/eusartcom.p1: Communication/eusartcom.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Communication" 
	@${RM} ${OBJECTDIR}/Communication/eusartcom.p1.d 
	@${RM} ${OBJECTDIR}/Communication/eusartcom.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Communication/eusartcom.p1 Communication/eusartcom.c 
	@-${MV} ${OBJECTDIR}/Communication/eusartcom.d ${OBJECTDIR}/Communication/eusartcom.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Communication/eusartcom.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Communication/spicom.p1: Communication/spicom.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Communication" 
	@${RM} ${OBJECTDIR}/Communication/spicom.p1.d 
	@${RM} ${OBJECTDIR}/Communication/spicom.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Communication/spicom.p1 Communication/spicom.c 
	@-${MV} ${OBJECTDIR}/Communication/spicom.d ${OBJECTDIR}/Communication/spicom.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Communication/spicom.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Controller/ADCController/adc_controller.p1: Controller/ADCController/adc_controller.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Controller/ADCController" 
	@${RM} ${OBJECTDIR}/Controller/ADCController/adc_controller.p1.d 
	@${RM} ${OBJECTDIR}/Controller/ADCController/adc_controller.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Controller/ADCController/adc_controller.p1 Controller/ADCController/adc_controller.c 
	@-${MV} ${OBJECTDIR}/Controller/ADCController/adc_controller.d ${OBJECTDIR}/Controller/ADCController/adc_controller.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Controller/ADCController/adc_controller.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Controller/DecoderController/decoder_controller.p1: Controller/DecoderController/decoder_controller.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Controller/DecoderController" 
	@${RM} ${OBJECTDIR}/Controller/DecoderController/decoder_controller.p1.d 
	@${RM} ${OBJECTDIR}/Controller/DecoderController/decoder_controller.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Controller/DecoderController/decoder_controller.p1 Controller/DecoderController/decoder_controller.c 
	@-${MV} ${OBJECTDIR}/Controller/DecoderController/decoder_controller.d ${OBJECTDIR}/Controller/DecoderController/decoder_controller.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Controller/DecoderController/decoder_controller.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Controller/operation_controller.p1: Controller/operation_controller.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Controller" 
	@${RM} ${OBJECTDIR}/Controller/operation_controller.p1.d 
	@${RM} ${OBJECTDIR}/Controller/operation_controller.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Controller/operation_controller.p1 Controller/operation_controller.c 
	@-${MV} ${OBJECTDIR}/Controller/operation_controller.d ${OBJECTDIR}/Controller/operation_controller.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Controller/operation_controller.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Controller/operations.p1: Controller/operations.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Controller" 
	@${RM} ${OBJECTDIR}/Controller/operations.p1.d 
	@${RM} ${OBJECTDIR}/Controller/operations.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Controller/operations.p1 Controller/operations.c 
	@-${MV} ${OBJECTDIR}/Controller/operations.d ${OBJECTDIR}/Controller/operations.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Controller/operations.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/helpers/i2c/i2c.p1: helpers/i2c/i2c.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/helpers/i2c" 
	@${RM} ${OBJECTDIR}/helpers/i2c/i2c.p1.d 
	@${RM} ${OBJECTDIR}/helpers/i2c/i2c.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/helpers/i2c/i2c.p1 helpers/i2c/i2c.c 
	@-${MV} ${OBJECTDIR}/helpers/i2c/i2c.d ${OBJECTDIR}/helpers/i2c/i2c.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/helpers/i2c/i2c.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/mcc_generated_files/examples/i2c2_master_example.p1: mcc_generated_files/examples/i2c2_master_example.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files/examples" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/examples/i2c2_master_example.p1.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/examples/i2c2_master_example.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/mcc_generated_files/examples/i2c2_master_example.p1 mcc_generated_files/examples/i2c2_master_example.c 
	@-${MV} ${OBJECTDIR}/mcc_generated_files/examples/i2c2_master_example.d ${OBJECTDIR}/mcc_generated_files/examples/i2c2_master_example.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/mcc_generated_files/examples/i2c2_master_example.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/mcc_generated_files/spi1.p1: mcc_generated_files/spi1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/spi1.p1.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/spi1.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/mcc_generated_files/spi1.p1 mcc_generated_files/spi1.c 
	@-${MV} ${OBJECTDIR}/mcc_generated_files/spi1.d ${OBJECTDIR}/mcc_generated_files/spi1.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/mcc_generated_files/spi1.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/mcc_generated_files/eusart.p1: mcc_generated_files/eusart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/eusart.p1.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/eusart.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/mcc_generated_files/eusart.p1 mcc_generated_files/eusart.c 
	@-${MV} ${OBJECTDIR}/mcc_generated_files/eusart.d ${OBJECTDIR}/mcc_generated_files/eusart.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/mcc_generated_files/eusart.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/mcc_generated_files/adc.p1: mcc_generated_files/adc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/adc.p1.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/adc.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/mcc_generated_files/adc.p1 mcc_generated_files/adc.c 
	@-${MV} ${OBJECTDIR}/mcc_generated_files/adc.d ${OBJECTDIR}/mcc_generated_files/adc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/mcc_generated_files/adc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/mcc_generated_files/i2c2_master.p1: mcc_generated_files/i2c2_master.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/i2c2_master.p1.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/i2c2_master.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/mcc_generated_files/i2c2_master.p1 mcc_generated_files/i2c2_master.c 
	@-${MV} ${OBJECTDIR}/mcc_generated_files/i2c2_master.d ${OBJECTDIR}/mcc_generated_files/i2c2_master.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/mcc_generated_files/i2c2_master.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/mcc_generated_files/device_config.p1: mcc_generated_files/device_config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/device_config.p1.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/device_config.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/mcc_generated_files/device_config.p1 mcc_generated_files/device_config.c 
	@-${MV} ${OBJECTDIR}/mcc_generated_files/device_config.d ${OBJECTDIR}/mcc_generated_files/device_config.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/mcc_generated_files/device_config.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/mcc_generated_files/interrupt_manager.p1: mcc_generated_files/interrupt_manager.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/interrupt_manager.p1.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/interrupt_manager.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/mcc_generated_files/interrupt_manager.p1 mcc_generated_files/interrupt_manager.c 
	@-${MV} ${OBJECTDIR}/mcc_generated_files/interrupt_manager.d ${OBJECTDIR}/mcc_generated_files/interrupt_manager.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/mcc_generated_files/interrupt_manager.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/mcc_generated_files/mcc.p1: mcc_generated_files/mcc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/mcc.p1.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/mcc.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/mcc_generated_files/mcc.p1 mcc_generated_files/mcc.c 
	@-${MV} ${OBJECTDIR}/mcc_generated_files/mcc.d ${OBJECTDIR}/mcc_generated_files/mcc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/mcc_generated_files/mcc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/mcc_generated_files/pin_manager.p1: mcc_generated_files/pin_manager.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/pin_manager.p1.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/pin_manager.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/mcc_generated_files/pin_manager.p1 mcc_generated_files/pin_manager.c 
	@-${MV} ${OBJECTDIR}/mcc_generated_files/pin_manager.d ${OBJECTDIR}/mcc_generated_files/pin_manager.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/mcc_generated_files/pin_manager.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Sensors/ECSensor/ec_sensor.p1: Sensors/ECSensor/ec_sensor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Sensors/ECSensor" 
	@${RM} ${OBJECTDIR}/Sensors/ECSensor/ec_sensor.p1.d 
	@${RM} ${OBJECTDIR}/Sensors/ECSensor/ec_sensor.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Sensors/ECSensor/ec_sensor.p1 Sensors/ECSensor/ec_sensor.c 
	@-${MV} ${OBJECTDIR}/Sensors/ECSensor/ec_sensor.d ${OBJECTDIR}/Sensors/ECSensor/ec_sensor.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Sensors/ECSensor/ec_sensor.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Sensors/HumidityTemperature/humid_sensor.p1: Sensors/HumidityTemperature/humid_sensor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Sensors/HumidityTemperature" 
	@${RM} ${OBJECTDIR}/Sensors/HumidityTemperature/humid_sensor.p1.d 
	@${RM} ${OBJECTDIR}/Sensors/HumidityTemperature/humid_sensor.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Sensors/HumidityTemperature/humid_sensor.p1 Sensors/HumidityTemperature/humid_sensor.c 
	@-${MV} ${OBJECTDIR}/Sensors/HumidityTemperature/humid_sensor.d ${OBJECTDIR}/Sensors/HumidityTemperature/humid_sensor.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Sensors/HumidityTemperature/humid_sensor.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Sensors/Oxygen/oxygen_sensor.p1: Sensors/Oxygen/oxygen_sensor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Sensors/Oxygen" 
	@${RM} ${OBJECTDIR}/Sensors/Oxygen/oxygen_sensor.p1.d 
	@${RM} ${OBJECTDIR}/Sensors/Oxygen/oxygen_sensor.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Sensors/Oxygen/oxygen_sensor.p1 Sensors/Oxygen/oxygen_sensor.c 
	@-${MV} ${OBJECTDIR}/Sensors/Oxygen/oxygen_sensor.d ${OBJECTDIR}/Sensors/Oxygen/oxygen_sensor.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Sensors/Oxygen/oxygen_sensor.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Sensors/PHSensor/ph_sensor.p1: Sensors/PHSensor/ph_sensor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Sensors/PHSensor" 
	@${RM} ${OBJECTDIR}/Sensors/PHSensor/ph_sensor.p1.d 
	@${RM} ${OBJECTDIR}/Sensors/PHSensor/ph_sensor.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Sensors/PHSensor/ph_sensor.p1 Sensors/PHSensor/ph_sensor.c 
	@-${MV} ${OBJECTDIR}/Sensors/PHSensor/ph_sensor.d ${OBJECTDIR}/Sensors/PHSensor/ph_sensor.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Sensors/PHSensor/ph_sensor.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Sensors/WaterLevel/waterlevel_sensor.p1: Sensors/WaterLevel/waterlevel_sensor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Sensors/WaterLevel" 
	@${RM} ${OBJECTDIR}/Sensors/WaterLevel/waterlevel_sensor.p1.d 
	@${RM} ${OBJECTDIR}/Sensors/WaterLevel/waterlevel_sensor.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Sensors/WaterLevel/waterlevel_sensor.p1 Sensors/WaterLevel/waterlevel_sensor.c 
	@-${MV} ${OBJECTDIR}/Sensors/WaterLevel/waterlevel_sensor.d ${OBJECTDIR}/Sensors/WaterLevel/waterlevel_sensor.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Sensors/WaterLevel/waterlevel_sensor.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Sensors/WaterTemperature/water_temp_sensor.p1: Sensors/WaterTemperature/water_temp_sensor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Sensors/WaterTemperature" 
	@${RM} ${OBJECTDIR}/Sensors/WaterTemperature/water_temp_sensor.p1.d 
	@${RM} ${OBJECTDIR}/Sensors/WaterTemperature/water_temp_sensor.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Sensors/WaterTemperature/water_temp_sensor.p1 Sensors/WaterTemperature/water_temp_sensor.c 
	@-${MV} ${OBJECTDIR}/Sensors/WaterTemperature/water_temp_sensor.d ${OBJECTDIR}/Sensors/WaterTemperature/water_temp_sensor.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Sensors/WaterTemperature/water_temp_sensor.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/types/queue/queue.p1: types/queue/queue.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/types/queue" 
	@${RM} ${OBJECTDIR}/types/queue/queue.p1.d 
	@${RM} ${OBJECTDIR}/types/queue/queue.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/types/queue/queue.p1 types/queue/queue.c 
	@-${MV} ${OBJECTDIR}/types/queue/queue.d ${OBJECTDIR}/types/queue/queue.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/types/queue/queue.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/main.p1: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.p1.d 
	@${RM} ${OBJECTDIR}/main.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/main.p1 main.c 
	@-${MV} ${OBJECTDIR}/main.d ${OBJECTDIR}/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/pic.p1: pic.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/pic.p1.d 
	@${RM} ${OBJECTDIR}/pic.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/pic.p1 pic.c 
	@-${MV} ${OBJECTDIR}/pic.d ${OBJECTDIR}/pic.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/pic.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/PIC16F1829.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -Wl,-Map=dist/${CND_CONF}/${IMAGE_TYPE}/PIC16F1829.${IMAGE_TYPE}.map  -D__DEBUG=1  -DXPRJ_default=$(CND_CONF)  -Wl,--defsym=__MPLAB_BUILD=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall -std=c99 -gdwarf-3 -mstack=compiled:auto:auto        $(COMPARISON_BUILD) -Wl,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -o dist/${CND_CONF}/${IMAGE_TYPE}/PIC16F1829.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/PIC16F1829.${IMAGE_TYPE}.hex 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/PIC16F1829.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -Wl,-Map=dist/${CND_CONF}/${IMAGE_TYPE}/PIC16F1829.${IMAGE_TYPE}.map  -DXPRJ_default=$(CND_CONF)  -Wl,--defsym=__MPLAB_BUILD=1    -fno-short-double -fno-short-float -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -msummary=-psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall -std=c99 -gdwarf-3 -mstack=compiled:auto:auto     $(COMPARISON_BUILD) -Wl,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -o dist/${CND_CONF}/${IMAGE_TYPE}/PIC16F1829.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
