#include "decoder_controller.h"
#include "../../mcc_generated_files/pin_manager.h"
#include <stdint.h>

void decoder_set_channel(uint8_t chan)
{
    chan &= (0b111);
    decoder_current_channel = chan;
    if (chan & 0b001)
    {
        DECODER_CHAN_1_SetHigh();
    }
    else
    {
        DECODER_CHAN_1_SetLow();
    }

    if (chan & 0b010)
    {
        DECODER_CHAN_2_SetHigh();
    }
    else
    {
        DECODER_CHAN_2_SetLow();
    }

    if (chan & 0b100)
    {
        DECODER_CHAN_3_SetHigh();
    }
    else
    {
        DECODER_CHAN_3_SetLow();
    }
    asm("NOP");
}