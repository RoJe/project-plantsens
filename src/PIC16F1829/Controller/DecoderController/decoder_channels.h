#ifndef DECODER_CHANNELS_H
#define DECODER_CHANNELS_H

typedef enum
{
    CHAN_NULL,//0
    CHAN_WATER_INLET,//1
    CHAN_WATER_OUTLET,//2
    CHAN_PH_UP,//3
    CHAN_PH_DOWN,//4
    CHAN_STOCK_A,//5
    CHAN_STOCK_B,//6
    CHAN_AIRVENT//7
} decoder_channel_t;

#endif