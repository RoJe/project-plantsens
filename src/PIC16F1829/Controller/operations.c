/*big ol case switch with all the things the microcontroller needs to do*/

#include <stdint.h>
#include "operations.h"

#include "../pic.h"

//INCLUDE FOR SENSORS
#include "../Sensors/ECSensor/ec_sensor.h"
#include "../Sensors/PHSensor/ph_sensor.h"
#include "../Sensors/HumidityTemperature/humid_sensor.h"
#include "../Sensors/Oxygen/oxygen_sensor.h"
#include "../Sensors/WaterTemperature/water_temp_sensor.h"

//INCLUDE FOR ACTUATORS
#include "../Actuators/Nutrients/nutrient_actuator.h"
#include "../Actuators/PH/ph_actuator.h"
#include "../Actuators/WaterLevel/waterlevel_actuator.h"
#include "../Actuators/AirVent/airvent_actuator.h"

void OPC_exec_op(uint8_t op)
{
    switch (op)
    {
        case (OP_PIC_HEARTBEAT):
            pic_heartbeat();
            break;
        case (OP_PIC_NOT_SUPPORTED):
            pic_not_supported();
            break;

        //WATERLEVEL CASES
        case (OP_WATERLVL_INLET_OPEN):
            inlet_open();
            break;
        case(OP_WATERLVL_INLET_CLOSE):
            inlet_close();
            break;
        case (OP_WATERLVL_OUTLET_OPEN):
            outlet_open();
            break;
        case (OP_WATERLVL_OUTLET_CLOSE):
            outlet_close();
            break;

        //NUTRIENTS CASES
        case (OP_NUTRIENT_SENSOR_ADD_TO_QUEUE):
            ec_read_sensor();
            break;
        case (OP_NUTRIENT_SEND_DATA):
            ec_transmit();
        case (OP_NUTRIENT_STOCKA_ON):
            stock_a_pump_enable();
            break;
        case (OP_NUTRIENT_STOCKA_OFF):
            stock_a_pump_disable();
            break;
        case (OP_NUTRIENT_STOCKB_ON):
            stock_b_pump_enable();
            break;
        case (OP_NUTRIENT_STOCKB_OFF):
            stock_b_pump_disable();
            break;

        //OXYGEN SENSOR CASES
        case (OP_OXYGEN_READ_SENSOR):
            oxygen_read_sensor();
            break;
        case (OP_OXYGEN_TEST_SENSOR):
            oxygen_sensor_test();
            break;
        case (OP_OXYGEN_PARSE_DATA):
            oxygen_parse_buffer();
            break;
        case (OP_OXYGEN_AIRFLOW_ENABLE):
            airvent_on();
            break;
        case (OP_OXYGEN_AIRFLOW_DISABLE):
            airvent_off();
            break;


        //WATER PH CASES
        case(OP_WATERPH_SENSOR_ADD_TO_QUEUE):
            ph_read_sensor();
            break;
        case (OP_WATERPH_SEND_DATA):
            ph_transmit();
            break;
        case (OP_WATERPH_DOWN_ON):
            phdown_pump_enable();
            break;
        case (OP_WATERPH_DOWN_OFF):
            phdown_pump_disable();
            break;
        case (OP_WATERPH_UP_ON):
            phup_pump_enable();
            break;
        case (OP_WATERPH_UP_OFF):
            phup_pump_disable();
            break;
        default:
            break;

        //WATER HUMIDITY OPS
        case (OP_HUMIDITY_SEND_ERROR):
            humid_send_error();
            break;
        case (OP_HUMIDITY_SEND_DATA):
            humid_send_data();
            break;
        case (OP_HUMIDITY_READ_SENSOR):
            humid_read_sensor();
            break;

        //WATER TEMPERATURE OPS
        case(OP_WATERTEMP_READ_SENSOR):
            water_temp_read_sensor();
            break;
        case (OP_WATERTEMP_SEND_DATA):
            water_temp_send_data();
            break;
        case (OP_WATERTEMP_SEND_ERROR):
            water_temp_send_error();
            break;

    }
}