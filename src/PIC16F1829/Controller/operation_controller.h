#ifndef OPERATION_CONTROLLER_H
#define OPERATION_CONTROLLER_H

#include <stdbool.h>
#include <stdint.h>

#include "../types/queue/queue.h"

#define OPC_QUEUE_SIZE 100
#define opc_type_t uint8_t

struct queue_t* opc_queue, opc_queue_;

void OPC_queue_init(void);
void OPC_put(uint16_t op);
void OPC_next_task(void);

#define OPC_pop(data) queue_pop(opc_queue, data)
#define OPC_queue_size() queue_size(opc_queue)
#define OPC_queue_empty() queue_empty(opc_queue)
#define OPC_queue_size() queue_size(opc_queue)

//controll if the OPC queue automatically gets refilled whenever its empty
bool OPC_auto_queue_refill;


#endif /*OPERATION_CONTROLLER_H*/