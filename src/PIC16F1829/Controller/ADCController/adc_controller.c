#include <stdbool.h>
#include <stdint.h>

#include "../../types/queue/queue.h"

//#include "adc_queue.h"
#include "adc_controller.h"
#include "../../Sensors/PHSensor/ph_sensor.h"
#include "../../Sensors/ECSensor/ec_sensor.h"
#include "../../mcc_generated_files/adc.h"

#define ADC_QUEUE_SIZE 3

void read_next_channel(void);
bool adc_running(void);
uint8_t adc_buff_[ADC_QUEUE_SIZE];

//channel that is currently being read by the adc
//is set to 0xFF when no channel is being read
uint8_t current_channel = 0xFF;

void adc_controller_init(void)
{
    adc_queue = &adc_queue_;
    queue_init(adc_queue, adc_buff_, ADC_QUEUE_SIZE, sizeof(uint8_t));
    ADC_SetInterruptHandler(adc_interrupt_handler);
}

void read_next_channel(void)
{
    adc_pop(&current_channel);
    ADC_SelectChannel(current_channel);
    ADC_StartConversion();
}

void adc_add_channel(uint8_t channel)
{
    //if adc is running, selecting next channel is handled by interrupts
    adc_put(channel);
    if (!adc_running())
    {
        read_next_channel();
    }
}

bool adc_running(void)
{
    return current_channel != 0xFF;
}

void adc_interrupt_handler(void)
{
    PIR1bits.ADIF = 0;
    switch (current_channel)
    {
        case (PH_SENSOR_CHANNEL):
            ph_on_sensor_read();
            break;
        case (EC_SENSOR_CHANNEL):
            ec_on_sensor_read();
            break;
        default:
            break;
    }
    if (!adc_queue_empty())
    {
        read_next_channel();
    }
    else
    {
        current_channel = 0xFF;
    }
}

void adc_put(uint8_t byte)
{
    queue_put(adc_queue, &byte);
}