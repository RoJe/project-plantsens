#ifndef ADC_CONTROLLER_H
#define ADC_CONTROLLER_H

#include "../../types/queue/queue.h"

struct queue_t* adc_queue, adc_queue_;

#define adc_pop(data) queue_pop(adc_queue, data)
#define adc_queue_size() queue_size(adc_queue)
#define adc_queue_empty() queue_empty(adc_queue)
void adc_put(uint8_t byte);


void adc_controller_init(void);
void adc_add_channel(uint8_t channel);
void adc_interrupt_handler(void);


#endif //ADC_CONTROLLER_H
