/*
 * File:   operation_controller.c
 * Author: paul
 *
 * Created on 3 mei 2020, 12:34
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "../types/queue/queue.h"

#include "../Communication/spicom.h"
#include "../Command/command_builder.h"
#include "operation_controller.h"
#include "operations.h"

/*
 * operation controller is resposnible for managing which proces starts
 * running when.
 * This includes preparing sensors, reading sensor data and sending it to the ESP
 * Effectively this is a queue that is called in the main loop of the program.
 * An function is pullen from the queue and then called
 */


uint16_t opc_queue_buffer[OPC_QUEUE_SIZE] = {0};

void OPC_queue_init(void)
{
    opc_queue = &opc_queue_;
    queue_init(opc_queue, opc_queue_buffer, OPC_QUEUE_SIZE, sizeof(uint8_t));
}

void OPC_put(uint16_t op)
{
    queue_put(opc_queue, &op);
}

void OPC_next_task(void)
{
    uint8_t cmd_byte; //byte passed to cmd_build_command
    uint16_t op;
    if (!spi_queue_empty())
    {
        if (spi_pop(&cmd_byte))
        {
            cmd_build_command(cmd_byte);
        }
    }
    if (!OPC_queue_empty())
    {
        OPC_pop(&op);
        OPC_exec_op(op);


        //GET SHIT FROM QUEUE AND EXECUTE
    }
    else if (OPC_auto_queue_refill)
    {
        //PUT SOME INSTRUCTIONS IN THE INSTRUCTION QUEUE
    }
}