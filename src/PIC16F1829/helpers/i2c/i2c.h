#ifndef I2C_H
#define I2C_H
#include <stdint.h>
#include <string.h>
#include <pic16f1829.h>
#define GLOBALS_HEADER

//#define  _XTAL_FREQ  	8000000 //8MHz
#define HIGH 1
#define LOW 0
// Define i2c pins
#define SDA		LATBbits.LATB4		// Data pin for i2c
#define SCK		LATBbits.LATB6		// Clock pin for i2c
#define SDA_DIR		TRISBbits.TRISB4        // Data pin direction
#define SCK_DIR		TRISBbits.TRISB6        // Clock pin direction

// Define i2c speed
#define I2C_SPEED   100000                  // 100kHz

//Function Declarations
void i2c_init(void);
void i2c_start(void);
void i2c_restart(void);
void i2c_stop(void);
void i2c_waitIDLE();
void i2c_sendACK(void);
void i2c_sendNACK(void);
unsigned char i2c_sendByte(unsigned char);
unsigned char i2c_readByte(unsigned char);

uint8_t I2C1_WriteByte(uint8_t i2c1SlaveAddress, uint8_t i2c1Data);
uint8_t I2C1_WriteFrame(uint8_t i2c1SlaveAddress, uint8_t* i2c1WritePointer, uint8_t i2c1FrameLength);
uint8_t I2C1_ReadFrame(uint8_t i2c1SlaveAddress, uint8_t* i2c1ReadPointer, uint8_t i2c1FrameLength);

#endif

