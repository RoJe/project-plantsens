#ifndef I2CCOM_H
#define I2CCOM_H

void I2C_Master_Init(const unsigned long clock);
void I2C_Master_Wait();
void I2C_Master_Start();
void I2C_Master_RepeatedStart();
void I2C_Master_Stop();
void I2C_Master_Write(unsigned data);
unsigned short I2C_Master_Read();

#endif //I2CCOM_H