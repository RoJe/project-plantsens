/*handles eusart communication with \r\n terminated strings*/

#include <stdint.h>
#include <stdbool.h>
#include <xc.h>

#include "eusartcom.h"
#include "../mcc_generated_files/eusart.h"
#include "../types/queue/queue.h"
#include "../Controller/operation_controller.h"
#include "../Controller/operations.h"

void on_string_complete(void);

uint8_t tx_queue_[TX_BUFF_SIZE];
uint8_t rx_queue_[RX_BUFF_SIZE];

uint8_t prev_char = '\0';

void eusart_tx_interrupt_handler(void)
{
    uint8_t data;
    if (!eusart_tx_queue_empty())
    {
        eusart_tx_pop(&data);
        TXREG = data;
    }
}

void eusart_transmit(char* str)
//transmits null terminated string over eusart
{
    char first_byte = *str;
    while (*str++)  //
    {
        eusart_tx_put(*str);
    }
    if (PIR1bits.TXIF == 0)
    {
        TXREG = first_byte;
    }
}

void eusart_rx_interrupt_handler(void)
{
    uint8_t byte = RCREG;
    eusart_rx_put(byte);
    if (byte == '\n' && prev_char == '\r')
    {
        if (eusartStringCompleteHandler)
        {
            eusartStringCompleteHandler();
        }
    }
    prev_char = byte;
}

void eusartcom_init(void)
{
    EUSART_SetTxInterruptHandler(eusart_tx_interrupt_handler);
    EUSART_SetRxInterruptHandler(eusart_rx_interrupt_handler);
    queue_init(tx_buff, tx_queue_, TX_BUFF_SIZE, sizeof(uint8_t));
    queue_init(rx_buff, rx_queue_, RX_BUFF_SIZE, sizeof(uint8_t));

    eusart_set_string_handler(on_string_complete);
}

void on_string_complete(void)
{

}

void eusart_set_string_handler(eusartStringCompleteHandler_t handler)
{
    eusartStringCompleteHandler = handler;
}