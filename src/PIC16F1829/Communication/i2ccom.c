#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <xc.h>

#include "i2ccom.h"

void I2C_Master_Init(const unsigned long clock)
{
    OPTION_REGbits.nWPUEN = 1;                                  //Enable single pin pull-up
    SSP2CON1 = 0b00101000;                                      //SSP Module as Master
    SSP2CON2 = 0;
    //SSP2ADD = (_XTAL_FREQ/(4*clock))-1;                       //Setting Clock Speed
    SSP2ADD = 0x09;
    SSP2STAT = 0;

    TRISBbits.TRISB5 = 1;                                       //Setting as input as given in datasheet
    TRISBbits.TRISB7 = 1;                                       //Setting as input as given in datasheet
    WPUBbits.WPUB7 = 1;
    WPUBbits.WPUB5 = 1;
}

void I2C_Master_Wait()
{
    while ((SSP2STAT & 0x04) || (SSP2CON2 & 0x1F));             //Transmit is in progress
}

void I2C_Master_Start()
{
    I2C_Master_Wait();
    SSP2CON2bits.SEN = 1;                                       //Initiate start condition
}

void I2C_Master_RepeatedStart()
{
    I2C_Master_Wait();
    SSP2CON2bits.RSEN = 1;                                      //Initiate repeated start condition
}

void I2C_Master_Stop()
{
    I2C_Master_Wait();
    SSP2CON2bits.PEN = 1;                                       //Initiate stop condition
}

void I2C_Master_Write(unsigned data)
{
    I2C_Master_Wait();
    SSP2BUF = data;                                             //Write data to SSPBUF
}

unsigned short I2C_Master_Read()
{
    unsigned short data;
    I2C_Master_Wait();
    SSP2CON2bits.RCEN = 1;
    I2C_Master_Wait();
    data = SSP2BUF;                                             //Read data from SSPBUF
    I2C_Master_Wait();
    SSP2CON2bits.ACKDT = 0;                                     //Acknowledge bit
    SSP2CON2bits.ACKEN = 1;                                     //Acknowledge sequence
    return data;
}