#ifndef SPICOM_H
#define SPICOM_H

#include <stdbool.h>
#include <stdint.h>

#include "../types/queue/queue.h"

struct queue_t* spi_buff, spi_buff_;
struct queue_t* spi_trans_buff, spi_trans_buff_;

void spicom_init(void);

void spi_put(uint8_t byte);
//bool spi_pop(uint8_t *data);
#define spi_pop(data) queue_pop(spi_buff, data)
#define spi_queue_size() queue_size(spi_buff)
#define spi_queue_empty() queue_empty(spi_buff)

void spi_trans_put(uint8_t byte);
uint8_t spi_trans_pop(uint8_t* data);
//#define spi_trans_pop(data) queue_pop(spi_trans_buff, data)
#define spi_trans_queue_size() queue_size(spi_trans_buff)
#define spi_trans_queue_empty() queue_empty(spi_trans_buff)

void spi_reply(uint8_t command, uint8_t* data, uint8_t size);

void spi_interupt_handler(void);

#endif //SPICOM_H