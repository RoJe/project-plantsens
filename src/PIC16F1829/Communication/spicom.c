#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <xc.h>

#include "../mcc_generated_files/spi1.h"
#include "../mcc_generated_files/pin_manager.h"
#include "spicom.h"

#define SPI_QUEUE_SIZE  10 //SIZE OF THE SPI QUEUE, UPTO 255
#define SPI_QUEUE_TRANS_SIZE 50
#define LONGEST_CMD 3

uint8_t spi_queue_[SPI_QUEUE_SIZE];
uint8_t spi_trans_queue_[SPI_QUEUE_TRANS_SIZE];

void spicom_init(void)
{
    SPI1_SetInterruptHandler(spi_interupt_handler);
    SPI1_Open(SPI1_DEFAULT);

    memset(spi_trans_queue_, '0', SPI_QUEUE_TRANS_SIZE);

    spi_buff = &spi_buff_;
    spi_trans_buff = &spi_trans_buff_;

    queue_init(spi_buff, spi_queue_, SPI_QUEUE_SIZE, sizeof(uint8_t));
    queue_init(spi_trans_buff, spi_trans_queue_, SPI_QUEUE_TRANS_SIZE, sizeof(uint8_t));
    //void queue_init(struct queue_t *queue, queue_size_t size, uint8_t data_size)
    SPI1_WriteByte(0x00);

    SPI_READY_PIN_SetDigitalOutput();
    SPI_READY_PIN_SetLow();
    //PORTCbits.RC5 = 0; //set spi data ready pin to low


}

void spi_interupt_handler(void)
{
    if (PIR1bits.SSP1IF == 1 && SSP1STATbits.BF)
    {
        //PORTCbits.RC0 = 1;
        PIR1bits.SSP1IF = 0;
        uint8_t byte = SPI1_ReadByte();
        uint8_t resp_byte;
        queue_put(spi_buff, &byte);
        if (queue_size(spi_trans_buff) != 0)
        {
            spi_trans_pop(&resp_byte);
            //queue_pop(spi_trans_buff, &resp_byte);
            SSP1BUF = resp_byte;
        }
        else
        {
            SSP1BUF = 0x00;
        }

    }
    else if (PIR2bits.BCL1IF == 1)
    {
        PIR2bits.BCL1IF = 0;
    }
}

void spi_trans_put(uint8_t byte)
{
    queue_put(spi_trans_buff, &byte);
    SPI_READY_PIN_SetHigh();
    //PORTCbits.RC5 = 1;

}

uint8_t spi_trans_pop(uint8_t* data)
{
    bool state = queue_pop(spi_trans_buff, data);
    if (spi_trans_queue_size() == 0)
    {
        SPI_READY_PIN_SetLow();
    }
    return state;
}

void spi_put(uint8_t byte)
{
    queue_put(spi_buff, &byte);
}

void spi_reply(uint8_t command, uint8_t* data, uint8_t size)
/*reply to a command from the controller
 *
 * size refers to the size of the data
 * 1 is added to size to accomodate the command byte
 * this means that data transmit size using this functionis limited to 244 bytes
 * which is more than the spi transmit buffer so stop complaining
 * */
{
    spi_trans_put(size+1);
    spi_trans_put(command);
    for(uint8_t i=0; i<size; i++)
    {
        spi_trans_put(data[i]);
    }

}