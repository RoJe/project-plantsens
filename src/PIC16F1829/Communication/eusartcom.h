
#ifndef EUSARTCOM_H
#define EUSARTCOM_H

#include <stdint.h>
#include <stdbool.h>

#define TX_BUFF_SIZE 20
#define RX_BUFF_SIZE 40 //size of the all character command type

#define eusart_rx_put(byte) queue_put(rx_buff, &byte)
#define eusart_rx_pop(data) queue_pop(rx_buff, data)
#define eusart_rx_queue_size() queue_size(rx_buff)
#define eusart_rx_queue_empty() queue_empty(rx_buff)
#define eusart_rx_queue_clear() queue_clear(rx_buff)

#define eusart_tx_put(byte) queue_put(tx_buff, &byte)
#define eusart_tx_pop(data) queue_pop(tx_buff, data)
#define eusart_tx_queue_size() queue_size(tx_buff)
#define eusart_tx_queue_empty() queue_empty(tx_buff)

typedef void (*eusartStringCompleteHandler_t)(void);
void (*eusartStringCompleteHandler)(void);

void eusart_set_string_handler(eusartStringCompleteHandler_t handler);
void eusart_transmit(char* str);
void eusartcom_init(void);

struct queue_t* tx_buff;
struct queue_t* rx_buff;

#endif //EUSARTCOM_H