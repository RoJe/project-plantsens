#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <xc.h>

#include "mcc_generated_files/mcc.h"
#include "mcc_generated_files/spi1.h"

#include "Communication/spicom.h"
#include "Communication/eusartcom.h"

#include "Controller/operation_controller.h"
#include "Controller/ADCController/adc_controller.h"

#include "Sensors/Oxygen/oxygen_sensor.h"

/*
Main application
 */


void setup(void)
{
    //PORTCbits.RC0 = 1;

    SYSTEM_Initialize();

    //INIT FOR COMMUNCATION CHANNELS
    spicom_init();
    eusartcom_init();

    //INIT FOR SENSORS (that need it)
    oxygen_sensor_init();

    //INIT FOR CONTROLLERS
    OPC_queue_init();
    adc_controller_init();

    //enable interrupts and clear interrupt flags
    INTERRUPT_GlobalInterruptEnable();
    INTERRUPT_PeripheralInterruptEnable();
    PIR1 = 0x00; //clear intterupt flags


}

void main(void)
{
    // initialize the device
    setup();

    //char str[] = "wtf u want\n";
    //spi_transmit(str, sizeof(str)-1);

    while (1)
    {
        OPC_next_task();
    }
}

