#ifndef QUEUE_H
#define QUEUE_H

#include <stdint.h>
#include <stdbool.h>

typedef uint8_t queue_size_t;

struct queue_t
{
    queue_size_t head;
    queue_size_t tail;
    queue_size_t size;
    uint8_t data_size;
    void* queue;
};

void queue_init(struct queue_t* queue, void* queue_buff, queue_size_t size, uint8_t data_size);
void queue_put(struct queue_t* queue, void* data);
bool queue_pop(struct queue_t* queue, void* data);
queue_size_t queue_size(struct queue_t* queue);
bool queue_empty(struct queue_t* queue);
void queue_clear(struct queue_t* queue);

#endif
