#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#include "queue.h"

void queue_init(struct queue_t* queue, void* queue_buff, queue_size_t size, uint8_t data_size)
{
    queue->head = 0;
    queue->tail = 0;
    queue->data_size = data_size; //amount of bytes in single data entry
    queue->size = size; //amount of items that can fit in the queue
    queue->queue = queue_buff;

}

void queue_put(struct queue_t* queue, void* data)
{
    queue_size_t size = queue->size;
    queue_size_t head = queue->head;
    queue_size_t tail = queue->tail;
    uint8_t data_size = queue->data_size;

    head = (head+data_size) % (size*data_size);
    memmove(queue->queue+head, data, data_size);
    if (head == tail)
    {
        tail = (tail+data_size) % (size*data_size);
    }
    queue->head = head;
}

bool queue_pop(struct queue_t* queue, void* data)
{
    queue_size_t size = queue->size;
    queue_size_t tail = queue->tail;
    uint8_t data_size = queue->data_size;

    if (!queue_empty(queue))
    {
        tail = (tail+data_size)%(size*data_size);
        if (data != NULL)  //don't write data if buffer points to NULL
        {
            memmove(data, queue->queue+tail, data_size);
        }
        queue->tail = tail;
        return true;
    }
    else
    {
        return false;
    }
}

queue_size_t queue_size(struct queue_t* queue)
{
    queue_size_t head, tail, size;
    uint8_t data_size;

    head = queue->head;
    tail = queue-> tail;
    size = queue-> size;
    data_size = queue->data_size;

    if (head == tail)
    {
        return 0;
    }
    else if (head > tail)
    {
        return (head - tail) / data_size;
    }
    else
    {
        return size - ((tail - head) / data_size);
    }
}

bool queue_empty(struct queue_t* queue)
{
    return queue->head == queue->tail;
}

void queue_clear(struct queue_t* queue)
{
    queue->head = 0;
    queue->tail = 0;
}
