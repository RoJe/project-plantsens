package main

import (
	"bufio"
	"crypto/tls"
	"encoding/json"
	"log"
	"math/rand"
	"net"
	"net/http"
	"net/textproto"
	"strings"
	"time"

	"github.com/gorilla/websocket"
)

var authToken = "asdjhagsjkdhgasjhdg"
var activeSystem = new(System)

// We'll need to define an Upgrader
// this will require a Read and Write buffer size
var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type authenticationMessage struct {
	Token string `json:"token"`
}

type System struct {
	farmH  map[string]Farmhand
	PlantS map[string]Plantsens
}

func (s *System) AddPlantsensConnection(id string) {
	if s.PlantS == nil {
		s.PlantS = make(map[string]Plantsens)
	}
	var tmp = new(Plantsens)
	tmp.Initialize()
	s.PlantS[id] = *tmp
}

func (s System) AddFarmhandConnection() int {
	return 0
}

type Farmhand struct {
	Code          string
	authenticated bool
	SendQue       map[string]Instructions
	RecQue        map[string]Instructions
}

type Plantsens struct {
	id            int
	Code          string
	authenticated bool
	canSend       bool
	Sensors       map[string]Sensor
	SendQue       map[string]Instructions
	RecQue        map[string]Instructions
	temp          int
	waterlevel    float64
}

type Sensor struct {
	Brand    string
	Instruct map[string]Instructions
}

type Instructions struct {
	Description    string                          `json:"-"`
	Name           string                          `json:"name"`
	Type           string                          `json:"type"`
	Data           interface{}                     `json:"data"`
	HandleDataFunc func(*Instructions)             `json:"-"`
	UpdateFunc     func(*Plantsens, *Instructions) `json:"-"`
}

func (p *Plantsens) Initialize() {
	p.initializeSensors()
	p.fillInstructions()
	p.fillSendQue()
	p.fillRecQue()

	p.canSend = true
}

func (p Plantsens) Sender(conn net.Conn) {
	defer conn.Close()
	for {
		jsonMessage, err := json.Marshal(p.getSendQueItem())
		if err != nil {
			log.Print(err)
			return
		}
		if _, err := conn.Write(jsonMessage); err != nil {
			log.Print(err)
		}
		time.Sleep(time.Millisecond * 500)
	}
}

func (p Plantsens) Reader(conn net.Conn) {
	defer conn.Close()

	buffer := make([]byte, 1024)
	for {
		n, err := conn.Read(buffer)
		message := string(buffer[:n])

		if err != nil {
			continue
		}
		var instr Instructions
		error := json.Unmarshal([]byte(message), &instr)
		if error != nil {
			continue
		}
		p.RecQue[instr.Type] = instr
		log.Print(p.RecQue[instr.Type])
	}

}

func (p *Plantsens) fillSendQue() {
	p.SendQue = make(map[string]Instructions)
	for _, sensor := range p.Sensors {
		for key, instr := range sensor.Instruct {
			if rand.Intn(10) == 5 {
				p.SendQue[key] = instr
			}
		}
	}
}
func (p *Plantsens) getSendQueItem() *Instructions {
	if len(p.SendQue) == 0 {
		p.initializeSensors()
		p.fillInstructions()
		p.fillSendQue()
	}
	var tmp *Instructions = nil
	for key, sendItem := range p.SendQue {
		tmp = &sendItem
		delete(p.SendQue, key)
		break
	}
	return tmp
}
func (p *Plantsens) fillRecQue() {
	p.RecQue = make(map[string]Instructions)
	for _, sensor := range p.Sensors {
		for key, _ := range sensor.Instruct {
			p.RecQue[key] = Instructions{}
		}
	}
}
func (p *Plantsens) initializeSensors() {
	p.Sensors = make(map[string]Sensor)

	p.Sensors["MicroController"] = Sensor{
		Brand:    "MPLAB",
		Instruct: make(map[string]Instructions),
	}
	p.Sensors["WaterLevel"] = Sensor{
		Brand:    "XXX",
		Instruct: make(map[string]Instructions),
	}
	p.Sensors["WaterPh"] = Sensor{
		Brand:    "XXX",
		Instruct: make(map[string]Instructions),
	}
	p.Sensors["WaterNutrient"] = Sensor{
		Brand:    "XXX",
		Instruct: make(map[string]Instructions),
	}
	p.Sensors["WaterTemperature"] = Sensor{
		Brand:    "XXX",
		Instruct: make(map[string]Instructions),
	}
	p.Sensors["GreenhouseOxygen"] = Sensor{
		Brand:    "XXX",
		Instruct: make(map[string]Instructions),
	}
	p.Sensors["GreenhouseClimate"] = Sensor{
		Brand:    "XXX",
		Instruct: make(map[string]Instructions),
	}
	p.Sensors["AutomationControl"] = Sensor{
		Brand:    "XXX",
		Instruct: make(map[string]Instructions),
	}
	p.Sensors["LEDcontroller"] = Sensor{
		Brand:    "XXX",
		Instruct: make(map[string]Instructions),
	}
}

func (p *Plantsens) fillInstructions() {
	var instructCol = make(map[string]map[string]Instructions)

	instructCol["MicroController"] = make(map[string]Instructions)
	instructCol["MicroController"]["Heartbeat"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "MicroController_Heartbeat",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["MicroController"]["EnterSlaveMode"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "MicroController_EnterSlaveMode",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["MicroController"]["Reset"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "MicroController_Reset",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["MicroController"]["DisableAutoSequence"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "MicroController_DisableAutoSequence",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["MicroController"]["EnableAutoSequence"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "MicroController_EnableAutoSequence",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}

	instructCol["WaterLevel"] = make(map[string]Instructions)
	instructCol["WaterLevel"]["GetData"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "WaterLevel_GetData",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["WaterLevel"]["SensorTest"] = Instructions{
		Description: "Message voor het opvragen van de status van de luchtsensor",
		Name:        "assignment",
		Type:        "WaterLevel_SensorTest",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["WaterLevel"]["WaterInValveOpen"] = Instructions{
		Description: "Message voor het opvragen van de status van de luchtsensor",
		Name:        "assignment",
		Type:        "WaterLevel_WaterInValveOpen",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["WaterLevel"]["WaterInValveClose"] = Instructions{
		Description: "Message voor het opvragen van de status van de luchtsensor",
		Name:        "assignment",
		Type:        "WaterLevel_WaterInValveClose",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["WaterLevel"]["WaterOutValveOpen"] = Instructions{
		Description: "Message voor het opvragen van de status van de luchtsensor",
		Name:        "assignment",
		Type:        "WaterLevel_WaterOutValveOpen",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["WaterLevel"]["WaterOutValveClose"] = Instructions{
		Description: "Message voor het opvragen van de status van de luchtsensor",
		Name:        "assignment",
		Type:        "WaterLevel_WaterOutValveClose",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}

	instructCol["WaterPh"] = make(map[string]Instructions)
	instructCol["WaterPh"]["GetData"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "WaterPh_GetData",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["WaterPh"]["SensorTest"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "WaterPh_SensorTest",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["WaterPh"]["PhUpPumpOn"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "WaterPh_PhUpPumpOn",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["WaterPh"]["PhDownPumpOff"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "WaterPh_PhDownPumpOff",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["WaterPh"]["PhDownPumpOn"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "WaterPh_PhDownPumpOn",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["WaterPh"]["PhUpPumpOff"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "WaterPh_PhUpPumpOff",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["WaterNutrient"] = make(map[string]Instructions)
	instructCol["WaterNutrient"]["GetData"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "WaterNutrient_GetData",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["WaterNutrient"]["TestSensor"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "WaterNutrient_TestSensor",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["WaterNutrient"]["StockAPumpOn"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "WaterNutrient_StockAPumpOn",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["WaterNutrient"]["StockAPumpOff"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "WaterNutrient_StockAPumpOff",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["WaterNutrient"]["StockBPumpOn"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "WaterNutrient_StockBPumpOn",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["WaterNutrient"]["StockBPumpOff"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "WaterNutrient_StockBPumpOff",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["WaterTemperature"] = make(map[string]Instructions)
	instructCol["WaterTemperature"]["GetData"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "WaterTemperature_GetData",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["WaterTemperature"]["SensorTest"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "WaterTemperature_SensorTest",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["WaterTemperature"]["EnableHeater"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "WaterTemperature_EnableHeater",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["WaterTemperature"]["DisableHeater"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "WaterTemperature_DisableHeater",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["GreenhouseOxygen"] = make(map[string]Instructions)
	instructCol["GreenhouseOxygen"]["GetData"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "GreenhouseOxygen_GetData",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["GreenhouseOxygen"]["SensorTest"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "GreenhouseOxygen_SensorTest",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["GreenhouseOxygen"]["EnableAirflow"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "GreenhouseOxygen_EnableAirflow",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["GreenhouseOxygen"]["DisableAirflow"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "GreenhouseOxygen_DisableAirflow",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["GreenhouseClimate"] = make(map[string]Instructions)
	instructCol["GreenhouseClimate"]["GetData"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "GreenhouseClimate_GetData",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["GreenhouseClimate"]["TestSensor"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "GreenhouseClimate_TestSensor",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["AutomationControl"] = make(map[string]Instructions)
	instructCol["AutomationControl"]["GetData"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "AutomationControl_GetData",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["AutomationControl"]["TestSensors"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "AutomationControl_TestSensors",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["AutomationControl"]["FillWaterTank"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "AutomationControl_FillWaterTank",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["LEDcontroller"] = make(map[string]Instructions)
	instructCol["LEDcontroller"]["setSpectrum"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "LEDcontroller_setSpectrum",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["LEDcontroller"]["setWhite"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "LEDcontroller_setWhite",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	instructCol["LEDcontroller"]["undoWhite"] = Instructions{
		Description: "Message voor het opvragen van de waarde van de luchtsensor",
		Name:        "assignment",
		Type:        "LEDcontroller_undoWhite",
		Data:        nil,
		HandleDataFunc: func(i *Instructions) {
			i.Data = nil
		},
		UpdateFunc: func(p *Plantsens, i *Instructions) {
			log.Print("Handle luchtsensor stuff")
		},
	}
	for sensorKey, sensor := range p.Sensors {
		sensor.Instruct = make(map[string]Instructions)
		for instrcColKey, instrcCol := range instructCol {
			if sensorKey == instrcColKey {
				for instrKey, instruct := range instrcCol {
					p.Sensors[sensorKey].Instruct[instrKey] = instruct
				}
			}
		}
	}
}

func (p *Plantsens) createRequest(sensor string, action string, value interface{}) Instructions {
	if value != nil {
		var tmp = p.Sensors[sensor].Instruct[action]
		tmp.Data = value
		p.Sensors[sensor].Instruct[action] = tmp
	}
	return p.Sensors[sensor].Instruct[action]
}

func main() {

	// http.HandleFunc("/", home_page)
	handlTLSSocket()
	// panic(http.ListenAndServe(":80", nil))
}

func handlTLSSocket() {
	log.SetFlags(log.Lshortfile)

	cer, err := tls.LoadX509KeyPair("server.crt", "server.key")
	if err != nil {
		log.Println(err)
		return
	}
	config := &tls.Config{Certificates: []tls.Certificate{cer}}
	ln, err := tls.Listen("tcp", ":6969", config)
	if err != nil {
		log.Println(err)
		return
	}
	defer ln.Close()
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Println(err)
			continue
		}
		go socket_init_conn(conn)
	}
}

func home_page(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "index.html")
}

func socket_init_conn(conn net.Conn) {
	reader := bufio.NewReader(conn)
	tp := textproto.NewReader(reader)
	for {
		// read one line (ended with \n or \r\n)
		line, _ := tp.ReadLine()
		line = strings.TrimSpace(string(line))
		if line == "nu1UXSkLU1i4SkZQg7fdc6H30i5x7bFt" {
			activeSystem.AddPlantsensConnection(line)
			go activeSystem.PlantS[line].Reader(conn)
			go activeSystem.PlantS[line].Sender(conn)
			break
		}
		// do something with data here, concat, handle and etc...
	}

	// }
}
