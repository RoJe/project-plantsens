 #!/bin/bash
project_folder=$PWD
src_folder="src"
config_file_extension="xml"
cpp_header_file_extension="h"
cpp_code_file_extension="cpp"
arduino_file_extension="ino"
c_file_extension="c"
backup_file_extension="orig"
uno_compiler="arduino:avr:uno"
esp32_compiler="esp32:esp32:esp32"

dest_folder="builds"
backup_folder="backups"
tests_folder="tests"


xc8_cc_arr=()


compile_arduino()
{
	declare device=$( xmlstarlet sel -t -v '/config/device' $1 )
	declare compiler=$( xmlstarlet sel -t -v '/config/compiler' $1 )
	declare buildfolder=$( xmlstarlet sel -t -v '/config/build-folder' $1 )
	declare buildfolderPre=$( xmlstarlet sel -t -v '/config/build-folder-pre' $1 )
	declare preParams=$( xmlstarlet sel -t -v '/config/pre/parameters' $1 )
	declare postParams=$( xmlstarlet sel -t -v '/config/post/parameters' $1 )
	declare base=$project_folder/$dest_folder/$buildfolder$(dirname $2)/
	base=${base/'./'/''}

	if arduino-cli compile --fqbn $compiler $2; then
		declare compiledFile=$2"."${compiler//":"/"."}
		if [ -f $compiledFile".hex" ]; then
			mv $compiledFile".hex" $base
		fi
		if [ -f $compiledFile".elf" ]; then
			mv $compiledFile".elf"  $base
		fi
		if [ -f $compiledFile".bin" ]; then
			mv $compiledFile".bin" $base
		fi	
	else
		exit 1
	fi
}

pre_compile_pic()
{
	declare device=$( xmlstarlet sel -t -v '/config/device' $1 )
	declare compiler=$( xmlstarlet sel -t -v '/config/compiler' $1 )
	declare buildfolder=$( xmlstarlet sel -t -v '/config/build-folder' $1 )
	declare buildfolderPre=$( xmlstarlet sel -t -v '/config/build-folder-pre' $1 )
	declare preParams=$( xmlstarlet sel -t -v '/config/pre/parameters' $1 )
	declare postParams=$( xmlstarlet sel -t -v '/config/post/parameters' $1 )

	mkdir -p $project_folder/$dest_folder/$buildfolder
	mkdir -p $project_folder/$dest_folder/$buildfolder$buildfolderPre

	if /opt/microchip/xc8/v2.20/bin/xc8-cc -mcpu=$device $preParams -o  $project_folder/$dest_folder/$buildfolder$buildfolderPre"$(basename "$2" | cut -d. -f1).p1" $2; then
		xc8_cc_arr+=($project_folder/$dest_folder/$buildfolder$buildfolderPre"$(basename "$2" | cut -d. -f1).p1")
	else
		exit 1
	fi
}
post_compile_pic()
{
	declare device=$( xmlstarlet sel -t -v '/config/device' $1 )
	declare compiler=$( xmlstarlet sel -t -v '/config/compiler' $1 )
	declare buildfolder=$( xmlstarlet sel -t -v '/config/build-folder' $1 )
	declare buildfolderPre=$( xmlstarlet sel -t -v '/config/build-folder-pre' $1 )
	declare preParams=$( xmlstarlet sel -t -v '/config/pre/parameters' $1 )
	declare postParams=$( xmlstarlet sel -t -v '/config/post/parameters' $1 )

	declare compileString=""
	for files in "${xc8_cc_arr[@]}"; do 
		compileString+=" ${files}"
	done
	if /opt/microchip/xc8/v2.20/bin/xc8-cc -mcpu=$device $postParams -o  $project_folder/$dest_folder/$buildfolder"main.elf" $compileString; then
		echo $compileString
	else
		exit 1
	fi

	xc8_cc_arr=()
}


linter_files_in_project()
{
	for file in $(find $src_folder -iname "*.$cpp_code_file_extension" -o -iname "*.$cpp_header_file_extension" -o -iname "*.$arduino_file_extension" -o -iname "*.$c_file_extension"); do 
		if [ -f $file ]; then
			astyle --style=allman --indent-col1-comments --add-braces --indent-classes --indent-switches --indent-preproc-block --indent-preproc-define --align-pointer=type $file
			if [ "${file##*.}" = $cpp_code_file_extension ]; then
				cppcheck --enable=all -x c++ --suppress=missingIncludeSystem  $file
			fi
			if [ "${file##*.}" = $c_file_extension ]; then
				cppcheck --enable=all -x c --suppress=missingIncludeSystem $file
			fi
			if [ "${file##*.}" = $cpp_header_file_extension ]; then
				cppcheck --enable=all -x c --suppress=missingIncludeSystem $file
			fi
		fi
	done
}

CI_main()
{
	linter_files_in_project

	mkdir -p $dest_folder
	mkdir -p $backup_folder

	for configs in $(find $src_folder -iname "*.$config_file_extension"); do
		cd $(dirname $configs)
		for file in $(find ./ -iname "*.$arduino_file_extension" -o -iname "*.$backup_file_extension" -o -type d -o -iname "*.$c_file_extension" ); do
			if [ -d $file -a "$file" != $src_folder ]; then
				mkdir -p $project_folder/$dest_folder$( dirname $(echo $configs | grep -oP "^($src_folder\K).*" ))/$(echo $file | grep -oP "^./\K.*")/
				mkdir -p $project_folder/$backup_folder$( dirname $(echo $configs | grep -oP "^($src_folder\K).*" ))/$(echo $file | grep -oP "^./\K.*")/
			fi
			if [ -f $file -a "${file##*.*.}" = $backup_file_extension ]; then
				mv $file $project_folder/$backup_folder$( dirname $(echo $configs | grep -oP "^($src_folder\K).*" ))/
			fi
			if [ -f $file -a "${file##*.*.}" = $c_file_extension ]; then
				pre_compile_pic "./config.xml" $file
			fi
			if [ -f $file -a "${file##*.}" = $arduino_file_extension ]; then
				compile_arduino "./config.xml" $file
			fi
		done
		if [ -n "$xc8_cc_arr" ]; then 
			post_compile_pic  "./config.xml"
		fi
		cd $project_folder
	done
}

CI_main
