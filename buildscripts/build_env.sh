#!/bin/bash
echo "===================================================="
echo "==      Build Enviourment script Plansens         =="
echo "===================================================="
echo "==           Created by Robbert de Jel            =="
echo "==                   Version 0.1                  =="
echo "===================================================="
echo "==             Updating installation..            =="
apt-get update &> /dev/null
echo "==                       Done                     =="
echo "===================================================="
cd ~
echo "==             Installing Astyle linter           =="
apt-get install astyle -y &> /dev/null
echo "==                       Done                     =="
echo "===================================================="
echo "==    Installing Cppcheck static code analysis    =="
apt-get install cppcheck -y &> /dev/null
echo "==                       Done                     =="
echo "===================================================="
echo "==                  Installing Curl               =="
apt-get install curl -y &> /dev/null
echo "==                       Done                     =="
echo "===================================================="
echo "==                 Installing Check               =="
apt-get install check -y &> /dev/null
echo "==                       Done                     =="
echo "===================================================="
echo "==                  Installing xml                =="
apt-get install xmlstarlet -y &> /dev/null
echo "==                       Done                     =="
echo "===================================================="
echo "==              Installing arduino-cli            =="
mkdir arduinoInstall
cd arduinoInstall
curl -L -o arduino-cli.tar.bz2 https://downloads.arduino.cc/arduino-cli/arduino-cli-latest-linux64.tar.bz2 &> /dev/null
tar xjf arduino-cli.tar.bz2 &> /dev/null
rm arduino-cli.tar.bz2 &> /dev/null
mv `ls -1` /usr/bin/
cd ../
echo "==                       Done                     =="
echo "===================================================="
echo "==                 Installing python              =="
apt-get install python -y &> /dev/null
echo "==                       Done                     =="
echo "===================================================="
echo "==                 Downloading pip                =="
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py &> /dev/null
echo "==                       Done                     =="
echo "===================================================="
echo "==             Running get-pip script             =="
python get-pip.py &> /dev/null
echo "==                       Done                     =="
echo "===================================================="
echo "==               Installing pyserial              =="
pip install pyserial &> /dev/null
echo "==                       Done                     =="
echo "===================================================="
echo "==                  Downloading XC8               =="
curl -L -o /tmp/xc8.run http://www.microchip.com/mplabxc8linux &> /dev/null 
echo "==                       Done                     =="
echo "===================================================="
echo "==               Running XC8 installer            =="
chmod a+x /tmp/xc8.run &> /dev/null 
/tmp/xc8.run --mode unattended --unattendedmodeui none --netservername localhost --LicenseType FreeMode &> /dev/null 
rm /tmp/xc8.run &> /dev/null 
echo "==                       Done                     =="
echo "===================================================="
echo "==       Downloading core files arduino-cli       =="
printf "board_manager:\n  additional_urls:\n    - https://dl.espressif.com/dl/package_esp32_index.json\n" > .arduino-cli.yaml
arduino-cli core update-index --config-file .arduino-cli.yaml &> /dev/null
arduino-cli core install esp32:esp32 --config-file .arduino-cli.yaml &> /dev/null
#install uno
arduino-cli core install arduino:avr --config-file .arduino-cli.yaml &> /dev/null
# Install 'native' packages
arduino-cli lib install "Adafruit BME280 Library" &> /dev/null
arduino-cli lib install "Adafruit Unified Sensor" &> /dev/null
arduino-cli lib install "HCSR04 ultrasonic sensor" &> /dev/null
arduino-cli lib install "ArduinoJson" &> /dev/null
arduino-cli lib install "MPU9250_asukiaaa" &> /dev/null
cd -
echo "==                       Done                     =="
echo "===================================================="
# Install 'third-party' packages: find proper location and 'git clone'
echo "==                  Installing git                =="
apt-get install git -y &> /dev/null
echo "==                       Done                     =="
echo "===================================================="
echo "==            Installing Aditional files          =="
cd `arduino-cli config dump | grep sketchbook | sed 's/.*\ //'`/libraries
git clone https://github.com/me-no-dev/AsyncTCP.git &> /dev/null
git clone https://github.com/me-no-dev/ESPAsyncWebServer.git &> /dev/null
git clone https://github.com/Links2004/arduinoWebSockets.git &> /dev/null


cd -
echo "==                       Done                     =="
echo "===================================================="

export PATH="$PATH:/opt/microchip/xc8/v2.20/bin/"
export PATH="$PATH:/opt/microchip/xc8/v2.20/pic/include" 